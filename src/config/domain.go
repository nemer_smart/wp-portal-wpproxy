package config

type Config struct {
	Log        *Log        `yaml:"log"`
	Server     *Server     `yaml:"server"`
	Simulation *Simulation `yaml:"simulation"`
}

type Log struct {
	Level string `yaml:"level" env:"LOG_LEVEL"`
}

type Server struct {
	Host            string `yaml:"host" env-required:"SERVER_HOST"`
	Port            string `yaml:"port" env-required:"SERVER_PORT"`
	NotificationURL string `yaml:"notification-url"`
}

type Simulation struct {
	// envia evento de rejeição se gross amount for como abaixo
	PaymentEventSentWaitTimeMillis          int     `yaml:"payment-eventsent-waittime-millis"`
	CheckoutEventSentWaitTimeMillis         int     `yaml:"checkout-eventsent-waittime-millis"`
	RejectPaymentGrossAmount                float64 `yaml:"reject-payment-gross-amount"`
	RejectCancellationGrossAmount           float64 `yaml:"reject-cancellation-gross-amount"`
	RejectExpenseGrossAmount                float64 `yaml:"reject-expense-gross-amount"`
	RejectEntireMoveAnyGrossAmount          float64 `yaml:"reject-entire-move-any-gross-amount"`
	RejectPaymentInMoveGrossAmount          float64 `yaml:"reject-payment-in-move-gross-amount"`
	RejectCancellationInMoveGrossAmount     float64 `yaml:"reject-cancellation-in-move-gross-amount"`
	RejectExpenseInMoveGrossAmount          float64 `yaml:"reject-expense-in-move-gross-amount"`
	RejectPaymentInCheckoutGrossAmount      float64 `yaml:"reject-payment-in-checkout-gross-amount"`
	RejectCancellationInCheckoutGrossAmount float64 `yaml:"reject-cancellation-in-checkout-gross-amount"`
	RejectExpenseInCheckoutGrossAmount      float64 `yaml:"reject-expense-in-checkout-gross-amount"`
	ErrorPaymentGrossAmount                 float64 `yaml:"error-payment-gross-amount"`
	ErrorCancellationGrossAmount            float64 `yaml:"error-cancellation-gross-amount"`
	ErrorExpenseGrossAmount                 float64 `yaml:"error-expense-gross-amount"`
	IgnorePaymentGrossAmount                float64 `yaml:"ignore-payment-gross-amount"`
	IgnoreCancellationGrossAmount           float64 `yaml:"ignore-cancellation-gross-amount"`
	IgnoreExpenseGrossAmount                float64 `yaml:"ignore-expense-gross-amount"`
	IgnoreMoveGrossAmount                   float64 `yaml:"ignore-move-gross-amount"`
	IgnoreCheckoutGrossAmount               float64 `yaml:"ignore-checkout-gross-amount"`
	ErrorMoveGrossAmount                    float64 `yaml:"error-move-gross-amount"`
	ErrorCheckoutGrossAmount                float64 `yaml:"error-checkout-gross-amount"`
	DefaultExchangeRate                     float64 `yaml:"default-exchange-rate"`
}
