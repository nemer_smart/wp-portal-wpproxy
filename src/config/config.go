package config

import (
	"github.com/ilyakaznacheev/cleanenv"
)

func New(cfgFilePath *string) (*Config, error) {

	cfg := new(Config)
	if err := cfg.readConfig(cfgFilePath); err != nil {
		return nil, err
	}
	return cfg, nil
}

func (cfg *Config) readConfig(cfgFilePath *string) error {

	if err := cleanenv.ReadConfig(*cfgFilePath, cfg); err != nil {
		return err
	}
	return nil
}

func (cfg *Config) GetServerConf() *Server {

	return &Server{
		Host:            cfg.Server.Host,
		Port:            cfg.Server.Port,
		NotificationURL: cfg.Server.NotificationURL,
	}
}
