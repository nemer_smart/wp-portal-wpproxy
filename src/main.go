package main

import (
	"bexstech/wpproxy/src/config"
	"bexstech/wpproxy/src/controller"
	"bexstech/wpproxy/src/usecase/notification"
	"flag"
	"fmt"
	"os"

	"bitbucket.org/bexstech/wpcore/wplog"
	"go.uber.org/dig"
)

const (
	Pid                   = "PID"
	DefaultConfigFilePath = "/etc/config/wp-portal-wpproxy.yaml"
)

var (
	pid         int
	log         = wplog.GetLogger("main")
	cfgFilePath = flag.String("c", DefaultConfigFilePath, "Path of configuration file")
	cfg         *config.Config
)

func init() {

	flag.Parse()
	pid = os.Getpid()

	var err error
	if cfg, err = config.New(cfgFilePath); err != nil {
		log.WithField(Pid, pid).WithError(err).Fatal("Couldn't start settings.")
	}
}

func main() {

	log.WithField(Pid, pid).Infof("Providing dependencies...")
	depManager, err := newDependencyManager()
	if err != nil {
		log.WithField(Pid, pid).WithError(err).Fatal("It was not possible to provide the dependencies.")
	}

	log.Infof("Initializing the application with PID: [%d]...", pid)
	ctrl, err := controller.New(depManager)
	if err != nil {
		log.WithField(Pid, pid).WithError(err).Errorf("Could not setup the application. PID: [%d].", pid)
	}
	if err = ctrl.Start(); err != nil {
		log.WithField(Pid, pid).WithError(err).Errorf("Could not start the application. PID: [%d].", pid)
	}
}

func GetConf() (*config.Config, error) {
	return cfg, nil
}

func newDependencyManager() (*dig.Container, error) {

	depManager := dig.New()

	if err := depManager.Provide(GetConf); err != nil {
		return nil, fmt.Errorf("Unable to inject config dependency. Detail: [%s].", err)
	}

	if err := depManager.Provide(cfg.GetServerConf); err != nil {
		return nil, fmt.Errorf("Unable to inject server config dependency. Detail: [%s].", err)
	}

	if err := depManager.Provide(mockEngeny); err != nil {
		return nil, fmt.Errorf("Unable to inject mock engeny  dependency. Detail: [%s].", err)
	}

	return depManager, nil
}

func mockEngeny(cfg *config.Config) (*notification.MockEngeny, error) {
	return notification.NewMockEngeny(cfg), nil
}
