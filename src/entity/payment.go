package entity

import (
	"time"
)

type Payment struct {
	PaymentRef
	PaymentOperation
	AdjustmentId     string          `json:"adjustmentId" firestore:"adjustmentId"`
	Date             *time.Time      `json:"date" firestore:"date"`
	EligibleForHedge bool            `json:"eligibleForHedge" firestore:"eligibleForHedge"`
	ExchangeRate     float64         `json:"exchangeRate" firestore:"exchangeRate"`
	LegacyId         string          `json:"cancelationId" firestore:"legacyIds"`
	QuoteRequest     string          `json:"quoteRequest" firestore:"quoteRequest"`
	CurrencyCode     CurrencyCode    `json:"currencyCode" firestore:"currencyCode"`
	PaymentType      PaymentType     `json:"paymentType" firestore:"paymentType"`
	ProductCategory  ProductCategory `json:"productCategory" firestore:"productCategory"`
	ExpenseType      ExpenseType     `json:"expenseType" firestore:"expenseType"`
	PaymentMethod    PaymentMethod   `json:"paymentMethod" firestore:"paymentMethod"`
	Merchant         Merchant        `json:"merchant" firestore:"merchant"`
	MerchantUsedPsp  bool            `json:"merchantUsedPsp" firestore:"merchantUsedPsp"`
	Psp              *Merchant       `json:"psp" firestore:"psp"`
	Consumer         *Consumer       `json:"consumer" firestore:"consumer"`

	Registry     *CheckoutRef  `json:"registry" firestore:"registry"`
	Settlement   *CheckoutRef  `json:"settlement" firestore:"settlement"`
	Bucket       *BucketRef    `json:"bucket" firestore:"bucket"`
	ContractInfo *ContractInfo `json:"contractInfo" firestore:"contractInfo"`
}

type PaymentRef struct {
	PaymentId string `json:"paymentId" firestore:"paymentId" bigquery:"paymentId"`
	PartnerId string `json:"partnerId" firestore:"partnerId" bigquery:"partnerId"`
}

type PaymentOperation struct {
	PaymentTrailer
	Amounts
	Errors *ValidationError `json:"errors,omitempty" firestore:"errors"`
}

type PaymentTrailer struct {
	Status    PaymentStatus `json:"status" firestore:"status"`
	CreatedAt *time.Time    `json:"createdAt" firestore:"createdAt"`
	UpdatedAt *time.Time    `json:"updatedAt" firestore:"updatedAt"`
	Version   int64         `json:"version" firestore:"version"`
}

type Amounts struct {
	ForeignGrossAmount  float64 `json:"foreignGrossAmount" firestore:"foreignGrossAmount" bigquery:"foreignGrossAmount"`
	NationalGrossAmount float64 `json:"nationalGrossAmount" firestore:"nationalGrossAmount" bigquery:"nationalGrossAmount"`
	ForeignNetAmount    float64 `json:"foreignNetAmount" firestore:"foreignNetAmount" bigquery:"foreignNetAmount"`
	NationalNetAmount   float64 `json:"nationalNetAmount" firestore:"nationalNetAmount" bigquery:"nationalNetAmount"`
}

type Merchant struct {
	Id               string         `json:"id,omitempty"`
	Name             string         `json:"name,omitempty"`
	CountryCode      string         `json:"countryCode,omitempty"`
	WebsiteURL       string         `json:"websiteUrl,omitempty"`
	SkipLimitControl bool           `json:"skipLimitControl,omitempty"`
	Status           MerchantStatus `json:"status,omitempty"`
}

type CheckoutRef struct {
	Id   string     `json:"id" firestore:"id"`
	Time *time.Time `json:"time" firestore:"time"`
}

type BucketRef struct {
	Name string     `json:"name" firestore:"name"`
	Time *time.Time `json:"time" firestore:"time"`
}

type ContractInfo struct {
	ContractNumber string  `json:"contractNumber" firestore:"contractNumber"`
	CappingRate    float64 `json:"cappingRate" firestore:"cappingRate"`
}

type PaymentList struct {
	Data       []*Payment
	Filter     map[string]interface{}
	OrderField string
	OffSet     int
	Limit      int
}

func (ccl *PaymentList) NewItem() interface{} {
	return new(Payment)
}

func (ccl *PaymentList) GetFilter() map[string]interface{} {
	return ccl.Filter
}

func (ccl *PaymentList) GetOrderField() string {
	return ccl.OrderField
}

func (ccl *PaymentList) GetOffSet() int {
	return ccl.OffSet
}

func (ccl *PaymentList) GetLimit() int {
	return ccl.Limit
}

func (ccl *PaymentList) Append(cc interface{}) {
	ccl.Data = append(ccl.Data, cc.(*Payment))
}
