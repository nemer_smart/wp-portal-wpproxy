package entity

import (
	"time"
)

const (
	SymbolicDeliveryMethod           DeliveryMethod = "SYMBOLIC"
	SwiftDeliveryMethod              DeliveryMethod = "SWIFT"
	BankAccountDeliveryMethod        DeliveryMethod = "BANK_ACCOUNT"
	ElectronicTransferDeliveryMethod DeliveryMethod = "ELECTRONIC_TRANSFER"
)

type SpreadType string
type RegistryMode string
type DeliveryMethod string

type CheckoutOperation struct {
	BucketOperation
	PartnerData
	Amounts
	BucketName string          `json:"bucketName" firestore:"bucketName"`
	Status     OperationStatus `json:"status" firestore:"status"`
	Stage      Stage           `json:"stage" firestore:"stage"`
}

type BucketOperation struct {
	Record
	OperationId string `json:"operationId" firestore:"operationId"`
	PartnerId   string `json:"partnerId" firestore:"partnerId"`
}

type Record struct {
	CreatedAt time.Time `json:"createdAt" firestore:"createdAt" bigquery:"createdAt"`
	UpdatedAt time.Time `json:"updatedAt" firestore:"updatedAt" bigquery:"updatedAt"`
}

type PartnerData struct {
	SplitPerMerchant  bool             `json:"splitPerMerchant" firestore:"splitPerMerchant"`
	BillTakeRate      bool             `json:"billTakeRate" firestore:"billTakeRate"`
	ProfileId         string           `json:"profileId" firestore:"profileId"`
	NationalAccountId string           `json:"nationalAccountId" firestore:"nationalAccountId"`
	ForeignAccountId  string           `json:"foreignAccountId" firestore:"foreignAccountId"`
	TakeRate          float64          `json:"takeRate" firestore:"takeRate"`
	DeliveryDate      map[string]int64 `json:"deliveryDate" firestore:"deliveryDate"`
}

// CHECKOUT EVENT
type BucketCheckout struct {
	Id         string     `json:"operationId" firestore:"operationId"`
	PartnerId  string     `json:"partnerId" firestore:"partnerId"`
	BucketName string     `json:"bucketName" firestore:"bucketName"`
	Status     Status     `json:"status" firestore:"status"`
	Stage      Stage      `json:"stage" firestore:"stage"`
	CreatedAt  *time.Time `json:"createdAt" firestore:"createdAt"`
}

func (cc *BucketCheckout) GetKey() string {
	return cc.Id
}

func (cc *BucketCheckout) SetKey(key string) {
	cc.Id = key
}

type BucketCheckoutList struct {
	Data       []*BucketCheckout
	Filter     map[string]interface{}
	OrderField string
	OffSet     int
	Limit      int
}

func (mpl *BucketCheckoutList) NewItem() interface{} {
	return new(BucketCheckout)
}

func (mpl *BucketCheckoutList) GetFilter() map[string]interface{} {
	return mpl.Filter
}

func (mpl *BucketCheckoutList) GetOrderField() string {
	return mpl.OrderField
}

func (mpl *BucketCheckoutList) GetOffSet() int {
	return mpl.OffSet
}

func (mpl *BucketCheckoutList) GetLimit() int {
	return mpl.Limit
}

func (mpl *BucketCheckoutList) Append(mp interface{}) {
	mpl.Data = append(mpl.Data, mp.(*BucketCheckout))
}

// MOVE EVENT
type BucketMove struct {
	Id              string        `json:"moveId"`
	PartnerId       string        `json:"partnerId"`
	TargetBucket    string        `json:"targetBucket"`
	OperationType   OperationType `json:"operationType"`
	PaymentIds      []string      `json:"paymentIds,omitempty"`
	CancellationIds []string      `json:"cancellationIds,omitempty"`
	LegacyIds       []string      `json:"legacyIds,omitempty"`
}

type PartnerSettings struct {
	PartnerId                  string                `json:"partnerId"`
	ProfileId                  string                `json:"profileId"`
	OfferType                  string                `json:"offerType"`
	FullName                   string                `json:"fullName"`
	DocumentNumber             string                `json:"documentNumber"`
	TradingName                string                `json:"tradingName"`
	SplitSettlementPerMerchant bool                  `json:"splitSettlementPerMerchant"`
	BillTakeRate               bool                  `json:"billTakeRate"`
	RegistryAutomatically      bool                  `json:"registryAutomatically"`
	RegistryMode               RegistryMode          `json:"registryMode"`
	RegistryTimeWindow         string                `json:"registryTimeWindow"`
	RegistryIntervalInMinutes  int64                 `json:"registryIntervalInMinutes"`
	NationalAccountId          string                `json:"nationalAccountId"`
	ForeignAccountId           string                `json:"foreignAccountId"`
	TakeRate                   float64               `json:"takeRate"`
	ForeignDeliveryMethod      DeliveryMethod        `json:"foreignDeliveryMethod"`
	NationalDeliveryMethod     DeliveryMethod        `json:"nationalDeliveryMethod"`
	DeliveryDate               map[string]int64      `json:"deliveryDate"`
	SpreadRate                 map[string]SpreadRate `json:"spreadRate"`
	WebHook                    WebHook               `json:"webHook"`
	IsInstantCheckout          bool                  `json:"isInstantCheckout"`
}

type SpreadRate struct {
	MarketOpen   float64 `json:"marketOpen"`
	MarketClosed float64 `json:"marketClosed"`
}

type AuthMode string

type ApiKey struct {
	Key string `json:"key" firestore:"key"`
}

type OAuth struct {
	AuthUrl      string `json:"authUrl" firestore:"authUrl"`
	ClientId     string `json:"clientId" firestore:"clientId"`
	ClientSecret string `json:"clientSecret" firestore:"clientSecret"`
	Audience     string `json:"audience" firestore:"audience"`
}

type WebHook struct {
	CallbackUrl string   `json:"callbackUrl" firestore:"callbackUrl"`
	AuthMode    AuthMode `json:"authMode" firestore:"authMode"`
	ApiKey      ApiKey   `json:"apiKey" firestore:"apiKey"`
	OAuth       OAuth    `json:"oAuth" firestore:"oAuth"`
}

// Bucket Balance Model
type BucketBalance struct {
	PayeeBalanceMap map[string]*PayeeBalance `json:"merchant_balance_map"`
}

type PayeeBalance struct {
	CoinBalanceMap map[string]*CoinBalance `json:"coin_balance_map"`
}

type CoinBalance struct {
	GoodsCredits          SellsBalance        `json:"goods_credits"`
	ServicesCredits       SellsBalance        `json:"service_credits"`
	TakeRateDebits        []TakeRateDebit     `json:"take_rate_debits"`
	ExpenseDebits         []ExpenseDebit      `json:"expense_debits"`
	CancellationDebits    []CancellationDebit `json:"cancellation_debits"`
	TotalCreditsRemaining float64             `json:"total_credits_remaining"`
	TotalDebitsRemaining  float64             `json:"total_debits_remaining"`
}

type SellsBalance struct {
	Credit               float64 `json:"credit"`
	TotalCreditRemaining float64 `json:"total_credit_remaining"`
}

type TakeRateDebit struct {
	Debit               float64         `json:"debit"`
	TotalDebitRemaining float64         `json:"total_debit_remaining"`
	ProductCategory     ProductCategory `json:"product_category"`
}

type ExpenseDebit struct {
	Debit               float64     `json:"debit"`
	TotalDebitRemaining float64     `json:"total_debit_remaining"`
	ExpenseType         ExpenseType `json:"expense_type"`
}

type CancellationDebit struct {
	Debit               float64         `json:"debit"`
	TotalDebitRemaining float64         `json:"total_debits_remaining"`
	ProductCategory     ProductCategory `json:"product_category"`
}
