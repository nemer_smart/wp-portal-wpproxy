package entity

import (
	"bitbucket.org/bexstech/wpcore/wperr"
	"errors"
	"fmt"
)

const (
	FieldErrorFormat   = "Field: %s - Error: %s"
	InvalidFieldFormat = "The field is invalid. Received: [%s]"
)

var (
	ErrorExpensesFullCancellation     = errors.New("Payments of type expenses accept just full cancellations.")
	ErrorFullCancelationAlReadyExists = errors.New("This payment already has a full cancellation.")
	ErrPartnerNotFound                = wperr.NewBusiness(wperr.NotFound, "partner not found")
)

type FieldError struct {
	Field   string
	Message error
	Abort   bool
}

func (e *FieldError) Error() string {
	return fmt.Sprintf(FieldErrorFormat, e.Field, e.Message)
}

type ValidationError struct {
	Status    ComplianceStatus  `json:"status,omitempty" sql:"status"`
	ErrorType string            `json:"errorType,omitempty" sql:"errorType"`
	Message   string            `json:"message,omitempty" sql:"message"`
	Details   map[string]string `json:"details,omitempty" sql:"details"`
}

func (ref ValidationError) Error() string {
	return fmt.Sprintf("%s - %s", ref.ErrorType, ref.Message)
}
