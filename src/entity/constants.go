package entity

const (
	CancellationTypeInvalid    CancellationType = ""
	CancellationTypeReversion  CancellationType = "REVERSION"
	CancellationTypeRefund     CancellationType = "REFUND"
	CancellationTypeUnregister CancellationType = "UNREGISTER"

	ConsumerTypeIndividual ConsumerType = "INDIVIDUAL"
	ConsumerTypeCompany    ConsumerType = "COMPANY"

	CurrencyCodeBRL CurrencyCode = "BRL"
	CurrencyCodeUSD CurrencyCode = "USD"
	CurrencyCodeGBP CurrencyCode = "GBP"
	CurrencyCodeEUR CurrencyCode = "EUR"
	CurrencyCodeCAD CurrencyCode = "CAD"

	ExpenseTypeLegal     ExpenseType = "LEGAL"
	ExpenseTypeMarketing ExpenseType = "MARKETING"
	ExpenseTypeShipment  ExpenseType = "SHIPMENT"
	ExpenseTypeFee       ExpenseType = "FEE"
	ExpenseTypeComission ExpenseType = "COMISSION"

	OperationStatusProcessing OperationStatus = "PROCESSING"
	OperationStatusCompleted  OperationStatus = "COMPLETED"

	OperationTypePayment      OperationType = "PAYMENT"
	OperationTypeCancellation OperationType = "CANCELLATION"
	OperationTypeLegacyIds    OperationType = "LEGACY_IDS"

	PaymentMethodCreditCard              PaymentMethod = "CREDIT_CARD"
	PaymentMethodDebitCard               PaymentMethod = "DEBIT_CARD"
	PaymentMethodElectronicFundsTransfer PaymentMethod = "ELECTRONIC_FUNDS_TRANSFER"
	PaymentMethodBankBillet              PaymentMethod = "BANK_BILLET"
	PaymentMethodVoucher                 PaymentMethod = "VOUCHER"
	PaymentMethodWallet                  PaymentMethod = "WALLET"
	PaymentMethodPix                     PaymentMethod = "PIX"
	PaymentMethodDomesticCard            PaymentMethod = "DOMESTIC_CARD"
	PaymentMethodOthers                  PaymentMethod = "OTHERS"

	PaymentTypePayin  PaymentType = "PAYIN"
	PaymentTypePayout PaymentType = "PAYOUT"

	ProductCategoryGoods    ProductCategory = "GOODS"
	ProductCategoryService  ProductCategory = "SERVICES"
	ProductCategoryExpenses ProductCategory = "EXPENSES"

	StageSettlement Stage = "SETTLEMENT"

	StatusScheduled Status = "SCHEDULED"
	StatusExecuted  Status = "EXECUTED"

	PaymentStatusAccepted               PaymentStatus = "ACCEPTED"
	PaymentStatusAvailableForRegistry   PaymentStatus = "AVAILABLE_REGISTRY"
	PaymentStatusAnalysing              PaymentStatus = "ANALYSING"
	PaymentStatusAvailableForSettlement PaymentStatus = "AVAILABLE_SETTLEMENT"
	PaymentStatusCompleted              PaymentStatus = "COMPLETED"
	PaymentStatusPendingRegistry        PaymentStatus = "PENDING_REGISTRY"
)

var (
	ValidStatusMap = map[PaymentStatus]bool{
		PaymentStatusAccepted:               true,
		PaymentStatusAvailableForRegistry:   true,
		PaymentStatusAnalysing:              true,
		PaymentStatusAvailableForSettlement: true,
		PaymentStatusCompleted:              true,
	}
	ValidStatusList = []string{
		string(PaymentStatusAccepted),
		string(PaymentStatusAvailableForRegistry),
		string(PaymentStatusAnalysing),
		string(PaymentStatusAvailableForSettlement),
		string(PaymentStatusCompleted),
	}
)

var (
	AllProductCategories = map[string]ProductCategory{
		string(ProductCategoryGoods):    ProductCategoryGoods,
		string(ProductCategoryService):  ProductCategoryService,
		string(ProductCategoryExpenses): ProductCategoryExpenses,
	}

	AllExpenseTypes = map[string]ExpenseType{
		string(ExpenseTypeLegal):     ExpenseTypeLegal,
		string(ExpenseTypeMarketing): ExpenseTypeMarketing,
		string(ExpenseTypeShipment):  ExpenseTypeShipment,
		string(ExpenseTypeFee):       ExpenseTypeFee,
	}
)

type (
	BucketStatus     string
	CancellationType string
	ComplianceStatus string
	ConsumerType     string
	CurrencyCode     string
	ExpenseType      string
	MerchantStatus   string
	OperationStatus  string
	OperationType    string
	ProductCategory  string
	PaymentMethod    string
	PaymentStatus    string
	PaymentType      string
	Stage            string
	Status           string
)
