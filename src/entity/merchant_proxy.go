package entity

import (
	"time"
)

type MerchantProxy struct {
	Key             string    `json:"key" firestore:"key"`
	PartnerID       string    `json:"partner_id" firestore:"partnerId"`
	MerchantID      string    `json:"merchant_id" firestore:"merchantId"`
	LegalName       string    `json:"legal_name" firestore:"legalName"`
	DocumentNumber  string    `json:"document_number" firestore:"documentNumber"`
	Time            time.Time `json:"create_at" firestore:"createAt"`
	ProductCategory string    `json:"product_category" firestore:"productCategory"`
}

func (mp *MerchantProxy) GetKey() string {
	return mp.Key
}

func (mp *MerchantProxy) SetKey(key string) {
	mp.Key = key
}

type MerchantProxyList struct {
	Data       []*MerchantProxy
	Filter     map[string]interface{}
	OrderField string
	OffSet     int
	Limit      int
}

func (mpl *MerchantProxyList) NewItem() interface{} {
	return new(MerchantProxy)
}

func (mpl *MerchantProxyList) GetFilter() map[string]interface{} {
	return mpl.Filter
}

func (mpl *MerchantProxyList) GetOrderField() string {
	return mpl.OrderField
}

func (mpl *MerchantProxyList) GetOffSet() int {
	return mpl.OffSet
}

func (mpl *MerchantProxyList) GetLimit() int {
	return mpl.Limit
}

func (mpl *MerchantProxyList) Append(mp interface{}) {
	mpl.Data = append(mpl.Data, mp.(*MerchantProxy))
}
