package entity

import (
	"time"
)

type Consumer struct {
	Type                   ConsumerType `json:"type,omitempty"`
	DocumentNumber         string       `json:"documentNumber,omitempty"`
	DateOfBirth            *time.Time   `json:"dateOfBirth,omitempty"`
	DateOfRegistry         *time.Time   `json:"dateOfRegistry,omitempty"`
	FirstName              string       `json:"firstName,omitempty"`
	LastName               string       `json:"lastName,omitempty"`
	Address                Address      `json:"address,omitempty"`
	Email                  string       `json:"email,omitempty"`
	Phone                  Phone        `json:"phone,omitempty"`
	DocumentValidationDate *time.Time   `json:"documentValidationDate,omitempty"`
}

type Address struct {
	Street       string `json:"street,omitempty"`
	Number       string `json:"number,omitempty"`
	Complement   string `json:"complement,omitempty"`
	Neighborhood string `json:"neighborhood,omitempty"`
	City         string `json:"cityName,omitempty"`
	CountryCode  string `json:"countryCode,omitempty"`
	State        string `json:"stateCode,omitempty"`
	ZipCode      string `json:"zipCode,omitempty"`
}

type Phone struct {
	Number      string `json:"number,omitempty"`
	AreaCode    string `json:"areaCode,omitempty"`
	CountryCode string `json:"countryCode,omitempty"`
	Provider    string `json:"provider,omitempty"`
}
