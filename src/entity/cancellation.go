package entity

type Cancellation struct {
	CancellationRef
	PaymentOperation
	Type          CancellationType `json:"type" firestore:"type"`
	Partially     bool             `json:"partially" firestore:"partially"`
	Ratio         float64          `json:"ratio" firestore:"ratio"`
	Registry      CheckoutRef      `json:"registry" firestore:"registry"`
	Settlement    CheckoutRef      `json:"settlement" firestore:"settlement"`
	Bucket        *BucketRef       `json:"bucket" firestore:"bucket"`
	FxOperationId string           `json:"fxOperationId" firestore:"fxOperationId"`
}

type CancellationRef struct {
	PaymentRef
	CancellationId string `json:"cancellationId" firestore:"cancellationId"`
}

func (cc *Cancellation) GetKey() string {
	return cc.CancellationId
}

func (cc *Cancellation) SetKey(key string) {
	cc.CancellationId = key
}

type CancellationList struct {
	Data       []*Cancellation
	Filter     map[string]interface{}
	OrderField string
	OffSet     int
	Limit      int
}

func (ccl *CancellationList) NewItem() interface{} {
	return new(Cancellation)
}

func (ccl *CancellationList) GetFilter() map[string]interface{} {
	return ccl.Filter
}

func (ccl *CancellationList) GetOrderField() string {
	return ccl.OrderField
}

func (ccl *CancellationList) GetOffSet() int {
	return ccl.OffSet
}

func (ccl *CancellationList) GetLimit() int {
	return ccl.Limit
}

func (ccl *CancellationList) Append(cc interface{}) {
	ccl.Data = append(ccl.Data, cc.(*Cancellation))
}
