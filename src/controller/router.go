package controller

import (
	"bexstech/wpproxy/src/controller/handlers"
	"net/http"

	"github.com/labstack/echo/v4"
	"go.uber.org/dig"
)

const (
	PaymentsPrefix         = "v1/payments"
	PaymentCancelEndpoint  = "/cancel/:payment-id"
	PaymentCaptureEndpoint = ""

	BucketsPrefix          = "v1/buckets"
	BucketMoveEndpoint     = "/:bucket-name/move"
	BucketCheckoutEndpoint = "/:bucket-name/checkout"
	BucketQueryPrefix      = "v1/query/buckets"
	BucketBalanceEndpoint  = "/:bucket-name/balance"

	MerchantPrefix   = "v1/merchants"
	MerchantEndpoint = "/:merchant-id"
)

type router struct {
	payments    *subRouter
	buckets     *subRouter
	bucketQuery *subRouter
	merchants   *subRouter
}

type subRouter struct {
	group  *echo.Group
	routes []*route
}

type route struct {
	method   string
	endpoint string
	f        func(c echo.Context) error
}

func newRouter(e *echo.Echo, depManager *dig.Container) (*router, error) {

	handler, err := handlers.New(depManager)
	if err != nil {
		return nil, err
	}

	return &router{
		payments: &subRouter{
			group: e.Group(PaymentsPrefix),
			routes: []*route{
				// Cancel
				{
					method:   http.MethodPost,
					endpoint: PaymentCancelEndpoint,
					f:        handler.Cancel.HandleCancel,
				},
				// Capture
				{
					method:   http.MethodPost,
					endpoint: PaymentCaptureEndpoint,
					f:        handler.Capture.CapturePayment,
				},
			},
		},
		buckets: &subRouter{
			group: e.Group(BucketsPrefix),
			routes: []*route{
				{
					method:   http.MethodPost,
					endpoint: BucketCheckoutEndpoint,
					f:        handler.Bucket.PostCheckout,
				},
				{
					method:   http.MethodPost,
					endpoint: BucketMoveEndpoint,
					f:        handler.Bucket.PostMove,
				},
			},
		},
		bucketQuery: &subRouter{
			group: e.Group(BucketQueryPrefix),
			routes: []*route{
				{
					method:   http.MethodGet,
					endpoint: BucketBalanceEndpoint,
					f:        handler.Bucket.GetBalance,
				},
			},
		},
		merchants: &subRouter{
			group: e.Group(MerchantPrefix),
			routes: []*route{
				{
					method:   http.MethodGet,
					endpoint: MerchantEndpoint,
					f:        handler.Merchant.GetMerchant,
				},
			},
		},
	}, nil
}

func (ref *router) build() {

	for _, route := range ref.payments.routes {
		ref.setRoute(ref.payments.group, route)
	}

	for _, route := range ref.buckets.routes {
		ref.setRoute(ref.buckets.group, route)
	}

	for _, route := range ref.bucketQuery.routes {
		ref.setRoute(ref.bucketQuery.group, route)
	}
	for _, route := range ref.merchants.routes {
		ref.setRoute(ref.merchants.group, route)
	}
}

func (ref *router) setRoute(g *echo.Group, r *route) {

	switch r.method {
	case http.MethodGet:
		g.GET(r.endpoint, r.f)

	case http.MethodPost:
		g.POST(r.endpoint, r.f)

	case http.MethodPut:
		g.PUT(r.endpoint, r.f)

	case http.MethodDelete:
		g.DELETE(r.endpoint, r.f)

	default:
		log.Errorf("Method not implemented. Received method: [%s].", r.method)
	}
}

func health(c echo.Context) error {

	return c.String(http.StatusOK, http.StatusText(http.StatusOK))
}
