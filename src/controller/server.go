package controller

import (
	"bexstech/wpproxy/src/config"
	"bitbucket.org/bexstech/wpcore/wplog"
	"bitbucket.org/bexstech/wpcore/wpstr"
	"github.com/go-playground/validator/v10"
	"github.com/labstack/echo/v4"
	"github.com/labstack/echo/v4/middleware"
	"go.uber.org/dig"
	"net/http"
)

var (
	log = wplog.GetLogger("controller")
)

type server struct {
	cfg    *config.Server
	e      *echo.Echo
	router *router
}

func getConfiguredRouter() (*echo.Echo, error) {
	echo.NotFoundHandler = func(e echo.Context) error {
		return e.JSON(http.StatusNotFound, map[string]interface{}{
			"code": http.StatusNotFound, "message": "Page not found",
		})
	}

	router := echo.New()

	router.Use(middleware.LoggerWithConfig(middleware.LoggerConfig{
		Skipper: func(ctx echo.Context) bool { return true },
		Format:  "method=${method}, uri=${uri}, status=${status}\n",
	}))
	router.Use(middleware.RequestIDWithConfig(middleware.RequestIDConfig{
		Skipper:   middleware.DefaultSkipper,
		Generator: wpstr.NewUuidV4,
	}))
	router.Use(middleware.Recover())

	return router, nil
}

func newServer(depManager *dig.Container) (*server, error) {
	var err error
	srv := new(server)
	if err = depManager.Invoke(func(cfgSrv *config.Server) {
		srv.cfg = cfgSrv
	}); err != nil {
		return nil, err
	}

	srv.e, err = getConfiguredRouter()
	if err != nil {
		return nil, err
	}

	r, err := newRouter(srv.e, depManager)
	if err != nil {
		return nil, err
	}
	srv.router = r
	return srv, nil
}

func (ref *server) Start() error {

	host := ref.cfg.Host + ref.cfg.Port
	log.Infof("Starting API server. Host: [%s]...", host)
	ref.setValidator()
	ref.router.build()

	return ref.e.Start(host)
}

func (ref *server) setValidator() {
	ref.e.Validator = &ParameterValidator{
		validator: validator.New(),
	}
}

type ParameterValidator struct {
	validator *validator.Validate
}

func (ref *ParameterValidator) Validate(i interface{}) error {
	return ref.validator.Struct(i)
}
