package handlers

import (
	"errors"
	"fmt"
	"net/http"

	"bitbucket.org/bexstech/wpcore/wperr"
	"bitbucket.org/bexstech/wpcore/wplog"
	"github.com/go-playground/validator/v10"
	"github.com/labstack/echo/v4"
	"go.uber.org/dig"
)

const (
	UuidKey              = "UUID"
	PartnerIdKey         = "userId"
	InvalidRequestFormat = "Your request is not valid. Check the details: [%v]."
	InvalidFieldFormat   = "The field is invalid. Received: %s."
	PatternFieldFormat   = " The accepted pattern is %s."
)

var (
	log = wplog.GetLogger("handlers")

	businessError   wperr.Business
	validationError validator.ValidationErrors
	patternFields   = map[string]string{
		"email":    "email@example.com",
		"number":   "[0123456789]",
		"required": "Must be informed.",
		"max":      "Exceed character limit.",
		"gt":       "A value greater than 0",
	}
)

type Handler struct {
	Bucket   *bucketHandler
	Cancel   *cancelHandler
	Capture  *captureHandler
	Merchant *merchantHandler
}

type binder interface {
	SetPartnerId(string)
}

func New(depManager *dig.Container) (*Handler, error) {

	var err error
	h := new(Handler)

	h.Bucket, err = newBucketHandler(depManager)
	if err != nil {
		return nil, err
	}

	h.Cancel, err = newCancelHandler(depManager)
	if err != nil {
		return nil, err
	}

	h.Capture, err = newCaptureHandler(depManager)
	if err != nil {
		return nil, err
	}

	h.Merchant, err = newMerchantHandler(depManager)
	if err != nil {
		return nil, err
	}
	return h, nil
}

func writeHeader(c echo.Context) {

	c.Response().Header().Set(echo.HeaderContentType, echo.MIMEApplicationJSONCharsetUTF8)
}

func bind(params binder, c echo.Context) error {

	db := new(echo.DefaultBinder)
	if err := db.Bind(params, c); err != nil {
		return wperr.Business{
			Code:    wperr.BadRequest,
			Message: fmt.Sprintf("Couldn't get parameters. Detail: [%v].", err),
		}
	}

	//partnerId := c.Get(PartnerIdKey)
	// considerando sempre partner do acelerador
	partnerId := "portal-partner-1"

	params.SetPartnerId(partnerId)
	return nil
}

func handleError(c echo.Context, err error) error {

	switch {
	case errors.As(err, &businessError):
		return handleBusinessError(c, err)

	case errors.As(err, &validationError):
		return handleValidationError(c, err)

	default:
		return echo.NewHTTPError(http.StatusInternalServerError, err)
	}
}

func handleBusinessError(c echo.Context, err error) error {

	e := err.(wperr.Business)
	switch e.Code {
	case wperr.BadRequest:
		return echo.NewHTTPError(http.StatusBadRequest, err)

	case wperr.Conflict:
		return echo.NewHTTPError(http.StatusConflict, err)

	case wperr.NotFound:
		return echo.NewHTTPError(http.StatusNotFound, err)

	default:
		return echo.NewHTTPError(http.StatusInternalServerError, err)
	}
}

func handleValidationError(c echo.Context, err error) error {

	list := err.(validator.ValidationErrors)
	errs := make(map[string]string)
	for _, err := range list {
		message := fmt.Sprintf(InvalidFieldFormat, err.Value())
		if pattern, ok := patternFields[err.Tag()]; ok {
			message += fmt.Sprintf(PatternFieldFormat, pattern)
		}
		errs[err.Field()] = message
	}

	e := wperr.Business{
		Code:    wperr.BadRequest,
		Message: fmt.Sprintf(InvalidRequestFormat, errs),
	}
	return echo.NewHTTPError(http.StatusBadRequest, e)
}
