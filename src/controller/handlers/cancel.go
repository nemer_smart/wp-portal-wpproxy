package handlers

import (
	"bexstech/wpproxy/src/usecase/cancel"
	"bitbucket.org/bexstech/wpcore/wpstr"
	"net/http"

	"github.com/labstack/echo/v4"
	"go.uber.org/dig"
)

type cancelHandler struct {
	service cancel.Interactor
}

func newCancelHandler(depManager *dig.Container) (*cancelHandler, error) {

	var err error
	handler := new(cancelHandler)
	handler.service, err = cancel.New(depManager)
	if err != nil {
		return nil, err
	}
	return handler, nil
}

// Payment cancellation
// @Summary Payment cancellation by payment id
// @Description Receives a partial or total cancellation request
// @Tags Payments
// @Accept json
// @Produce json
// @Param payment body entity cancel.Request true
// @Success 201 {object} no content
// @Failure 400 {object} wperr.Business
// @Failure 404 {object} wperr.Business
// @Failure 500 {object} wperr.Business
// @Security BearerToken
// @Router /cancel/{payment-id} [post]
func (ref *cancelHandler) HandleCancel(c echo.Context) error {

	uuid := wpstr.NewUuidV4()
	log.WithField(UuidKey, uuid).Debugf("Request received. Url: [%s].", c.Path())

	params := new(cancel.Params)
	if err := bind(params, c); err != nil {
		return handleError(c, err)
	}

	if err := c.Validate(params); err != nil {
		return handleError(c, err)
	}

	params.Id = uuid
	if err := ref.service.DispatchCancel(c.Request().Context(), params); err != nil {
		log.WithField(UuidKey, uuid).WithError(err).Error("Payment cancel could not be dispatched.")
		return handleError(c, err)
	}

	log.WithField(UuidKey, uuid).Debugf("Status code: [%d].", http.StatusCreated)
	return c.JSON(http.StatusCreated, echo.Map{"cancellationId": uuid})
}
