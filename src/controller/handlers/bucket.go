package handlers

import (
	"bexstech/wpproxy/src/usecase/bucket/checkout"
	"bexstech/wpproxy/src/usecase/bucket/move"
	"context"
	"net/http"

	"github.com/labstack/echo/v4"
	"go.uber.org/dig"
)

const defaultBucket = "inbox_settlement"

type bucketHandler struct {
	checkSrv checkout.Interactor
	moveSrv  move.Interactor
}

func newBucketHandler(depManager *dig.Container) (*bucketHandler, error) {

	var err error
	handler := new(bucketHandler)
	handler.checkSrv, err = checkout.New(depManager)
	handler.moveSrv, err = move.New(depManager)
	if err != nil {
		return nil, err
	}
	return handler, nil
}

func (ref *bucketHandler) PostCheckout(c echo.Context) error {

	uuid := c.Response().Header().Get(echo.HeaderXRequestID)
	log.WithField(UuidKey, uuid).Debugf("Request received. Url: [%s].", c.Path())

	params := new(checkout.CheckoutParams)
	params.Ctx = context.WithValue(context.Background(), UuidKey, uuid)
	if err := bind(params, c); err != nil {
		return handleError(c, err)
	}

	log.WithField(UuidKey, uuid).Debug("Processing the request...")
	response, err := ref.checkSrv.SetBucketCheckout(params)
	if err != nil {
		log.WithField(UuidKey, uuid).WithError(err).Error("Bucket checkout could not be registered.")
		return handleError(c, err)
	}

	log.WithField(UuidKey, uuid).Debugf("Responding to successful request. Status code: [%d].", http.StatusCreated)
	writeHeader(c)
	return c.JSON(http.StatusCreated, response)
}

func (ref *bucketHandler) PostMove(c echo.Context) error {

	uuid := c.Response().Header().Get(echo.HeaderXRequestID)
	log.WithField(UuidKey, uuid).Debugf("Request received. Url: [%s].", c.Path())

	params := new(move.MoveParams)
	params.Ctx = context.WithValue(context.Background(), UuidKey, uuid)
	if err := bind(params, c); err != nil {
		return handleError(c, err)
	}

	log.WithField(UuidKey, uuid).Debug("Processing the request...")
	response, err := ref.moveSrv.SetBucketMove(params)
	if err != nil {
		log.WithField(UuidKey, uuid).WithError(err).Error("Bucket move could not be registered.")
		return handleError(c, err)
	}

	log.WithField(UuidKey, uuid).Debugf("Responding to successful request. Status code: [%d].", http.StatusCreated)
	writeHeader(c)
	return c.JSON(http.StatusCreated, response)
}

func (ref *bucketHandler) GetBalance(c echo.Context) error {
	uuid := c.Response().Header().Get(echo.HeaderXRequestID)
	log.WithField(UuidKey, uuid).Debugf("Request received. Url: [%s].", c.Path())
	params := new(move.BucketBalanceParams)
	params.Ctx = context.WithValue(context.Background(), UuidKey, uuid)
	if err := bind(params, c); err != nil {
		return handleError(c, err)
	}
	response := ref.moveSrv.GetBucketBalance(params)

	log.WithField(UuidKey, uuid).Debugf("Responding to successful request. Status code: [%d].", http.StatusCreated)
	writeHeader(c)
	return c.JSON(http.StatusCreated, response)
}
