package handlers

import (
	"bexstech/wpproxy/src/usecase/capture"
	"bitbucket.org/bexstech/wpcore/wperr"
	"context"
	"net/http"

	"github.com/labstack/echo/v4"
	"go.uber.org/dig"
)

type captureHandler struct {
	service capture.Interactor
}

var merchants = map[string]string{
	"Portal 1": "76a2daaf-9085-40ea-99aa-bd70cd458577",
	"Portal 2": "cca382db-9ccb-410d-ae0c-5ffcb11320a9",
}

func newCaptureHandler(depManager *dig.Container) (*captureHandler, error) {

	var err error
	handler := new(captureHandler)
	handler.service, err = capture.New(depManager)
	if err != nil {
		return nil, err
	}
	return handler, nil
}

// Register payment
// @Summary Register payment
// @Description Receives and accrue a financial operation to be subsequently registered and settled
// @Tags Payments
// @Accept json
// @Produce json
// @Param payment body entity capture.PaymentInput true
// @Success 201 {object} entity.PaymentReference
// @Failure 400 {object} wperr.Business
// @Failure 500 {object} wperr.Business
// @Security BearerToken
// @Router / [post]
func (ref *captureHandler) CapturePayment(c echo.Context) error {

	uuid := c.Response().Header().Get(echo.HeaderXRequestID)
	log.WithField(UuidKey, uuid).Debugf("Request received. Url: [%s].", c.Path())

	params := new(capture.Params)
	params.Ctx = context.WithValue(context.Background(), UuidKey, uuid)
	if err := bind(params, c); err != nil {
		return handleError(c, err)
	}
	// ingerir nos Merchants
	mln := params.MerchantLegalName
	merchantId, ok := merchants[mln]
	if !ok {
		return c.JSON(http.StatusBadRequest, wperr.Business{
			Message: "Invalid Merchant Legal Name - Not Found",
			Code:    400,
		})
	}

	params.MerchantId = merchantId
	if err := c.Validate(params); err != nil {
		return handleError(c, err)
	}
	log.WithField(UuidKey, uuid).Debug("Processing the request...")
	response, err := ref.service.DispatchPayment(params)
	if err != nil {
		log.WithField(UuidKey, uuid).WithError(err).Error("Payment capture could not be registered.")
		return handleError(c, err)
	}

	log.WithField(UuidKey, uuid).Debugf("Responding to successful request. Status code: [%d].", http.StatusCreated)
	writeHeader(c)
	return c.JSON(http.StatusCreated, response.Data)
}
