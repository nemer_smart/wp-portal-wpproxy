package handlers

import (
	"bexstech/wpproxy/src/usecase/merchant"
	"net/http"

	"github.com/labstack/echo/v4"
	"go.uber.org/dig"
)

type merchantHandler struct {
}

func newMerchantHandler(depManager *dig.Container) (*merchantHandler, error) {

	var err error
	handler := new(merchantHandler)

	if err != nil {
		return nil, err
	}
	return handler, nil
}

func (ref *merchantHandler) GetMerchant(c echo.Context) error {

	uuid := c.Response().Header().Get(echo.HeaderXRequestID)
	log.WithField(UuidKey, uuid).Debugf("Request received. Url: [%s].", c.Path())

	params := new(merchant.Params)

	if err := bind(params, c); err != nil {
		return handleError(c, err)
	}

	log.WithField(UuidKey, uuid).Debug("Processing the request...")
	merchant, ok := merchant.ValidMerchants[params.MerchantId]
	if !ok {
		return c.JSON(http.StatusCreated, map[string]interface{}{"code": 2, "message": "NOT FOUND"})
	}
	log.WithField(UuidKey, uuid).Debugf("Responding to successful request. Status code: [%d].", http.StatusCreated)
	writeHeader(c)
	return c.JSON(http.StatusCreated, merchant)
}
