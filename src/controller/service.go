package controller

import (
	"go.uber.org/dig"
)

type ServerService interface {
	Start() error
}

func New(depManager *dig.Container) (ServerService, error) {

	return newServer(depManager)
}
