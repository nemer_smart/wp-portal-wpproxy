package merchant

var ValidMerchants = map[string]Merchant{
	"76a2daaf-9085-40ea-99aa-bd70cd458577": {
		ID:          "76a2daaf-9085-40ea-99aa-bd70cd458577",
		CallbackURL: "",
		Company: Company{
			BusinessName:              "Portal 1",
			CompanyRegistrationNumber: "",
			DateOfIncorporation:       "",
			GoodsDelivery:             nil,
			LegalName:                 "Portal 1",
			License:                   "",
			PlaceOfIncorporation:      "",
			ShareCapital:              ShareCapital{},
			TaxPayerIdentification:    "",
			Website:                   "",
		},
		OfferType:        "",
		DocumentNumber:   "",
		PartnerID:        "portal-partner-1",
		ProfileType:      "",
		RoleType:         "",
		ProductCategory:  "",
		ForeignAccountID: "",
	},
	"cca382db-9ccb-410d-ae0c-5ffcb11320a9": {
		ID:          "cca382db-9ccb-410d-ae0c-5ffcb11320a9",
		CallbackURL: "",
		Company: Company{
			BusinessName:              "Portal 2",
			CompanyRegistrationNumber: "",
			DateOfIncorporation:       "",
			GoodsDelivery:             nil,
			LegalName:                 "Portal 2",
			License:                   "",
			PlaceOfIncorporation:      "",
			ShareCapital:              ShareCapital{},
			TaxPayerIdentification:    "",
			Website:                   "",
		},
		OfferType:        "",
		DocumentNumber:   "",
		PartnerID:        "portal-partner-1",
		ProfileType:      "",
		RoleType:         "",
		ProductCategory:  "",
		ForeignAccountID: "",
	},
}

type Params struct {
	PartnerId  string `json:"-"`
	MerchantId string `param:"merchant-id"`
}

func (cp *Params) SetPartnerId(partnerId string) {
	cp.PartnerId = partnerId
}

type Merchant struct {
	ID               string
	CallbackURL      string  `json:"callback_url"`
	Company          Company `json:"company"`
	OfferType        string  `json:"offer_type"`
	DocumentNumber   string  `json:"document_number"`
	PartnerID        string  `json:"partner_id"`
	ProfileType      string  `json:"profile_type"`
	RoleType         string  `json:"role_type"`
	ProductCategory  string  `json:"product_category"`
	ForeignAccountID string  `json:"foreign_account_id"`
}

type Company struct {
	BusinessName              string         `json:"business_name" firestore:"businessName"`
	CompanyRegistrationNumber string         `json:"company_registration_number" firestore:"companyRegistrationNumber"`
	DateOfIncorporation       string         `json:"date_of_incorporation,omitempty" firestore:"dateOfIncorporation"`
	GoodsDelivery             *GoodsDelivery `json:"goods_delivery" firestore:"goodsDelivery"`
	LegalName                 string         `json:"legal_name" validate:"required" firestore:"legalName"`
	License                   string         `json:"license" firestore:"license"`
	PlaceOfIncorporation      string         `json:"place_of_incorporation" firestore:"placeOfIncorporation"`
	ShareCapital              ShareCapital   `json:"share_capital" firestore:"shareCapital"`
	TaxPayerIdentification    string         `json:"tax_payer_identification" firestore:"taxPayerIdentification"`
	Website                   string         `json:"website" firestore:"website"`
}

type GoodsDelivery struct {
	AverageDeliveryDays   int    `json:"average_delivery_days" firestore:"averageDeliveryDays"`
	Insurance             bool   `json:"insurance" firestore:"insurance"`
	ShippingMethods       string `json:"shipping_methods" firestore:"shippingMethods"`
	TrackingCodeAvailable bool   `json:"tracking_code_available" firestore:"trackingCodeAvailable"`
}

type ShareCapital struct {
	Amount   float64 `json:"amount" firestore:"amount"`
	Currency string  `json:"currency" firestore:"currency"`
}

type Error struct {
	Error string `json:"error,omitempty"`
}
