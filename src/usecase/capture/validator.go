package capture

import (
	"bexstech/wpproxy/src/entity"
	"bitbucket.org/bexstech/wpcore/wperr"
	"bitbucket.org/bexstech/wpcore/wpstr"
	"errors"
	"fmt"
	"github.com/shopspring/decimal"
	"regexp"
	"sort"
	"time"
)

const (
	MaxForeignGrossAmount = float64(3000)
)

var (
	ErrorEmptyFirstName           = errors.New("The first_name cannot be empty.")
	ErrorEmptyLastName            = errors.New("The last_name cannot be empty.")
	ErrorEmptyDate                = errors.New("The date cannot be empty. The accepted format is: YYYY-MM-DD.")
	ErrorInvalidMethodForServices = errors.New("The provided payment method is not allowed for productCategory SERVICES.")
	ErrorShipmentForExpenses      = errors.New("SHIPMENT is not yet available for productCategory EXPENSES.")
	ErrorPayMethodForExpenses     = errors.New("The accepted payment method for productCategory EXPENSES is ELECTRONIC_FUNDS_TRANSFER.")
	ErrorPaymentTypeExpenses      = errors.New("the payment type of an expense can`t be PAYIN")
	ErrorPaymentTypeGoods         = errors.New("payment with product category GOODS can`t be a PAYOUT")
	ErrorPaymentTypeServices      = errors.New("payment with product category SERVICES can`t be a PAYOUT")
)

type validator struct {
	list []Validator
}

type Validator interface {
	Validate(*Params, map[string]error) bool
}

type (
	AmountsValidator struct{}

	ConsumerValidator struct {
		acceptedValues map[string]entity.ConsumerType
	}

	CurrencyCodeValidator struct {
		acceptedValues map[string]entity.CurrencyCode
	}

	MerchantValidator struct {
	}

	PaymentDateValidator struct{}

	PaymentLabelsValidator struct {
		paymentTypeValues   map[string]entity.PaymentType
		expenseTypeValues   map[string]entity.ExpenseType
		paymentMethodValues map[string]entity.PaymentMethod
	}
)

func newValidator() *validator {

	return &validator{
		list: []Validator{
			&AmountsValidator{},
			&ConsumerValidator{
				acceptedValues: map[string]entity.ConsumerType{
					string(entity.ConsumerTypeIndividual): entity.ConsumerTypeIndividual,
					string(entity.ConsumerTypeCompany):    entity.ConsumerTypeCompany,
				},
			},
			&CurrencyCodeValidator{
				acceptedValues: map[string]entity.CurrencyCode{
					string(entity.CurrencyCodeUSD): entity.CurrencyCodeUSD,
					string(entity.CurrencyCodeGBP): entity.CurrencyCodeGBP,
					string(entity.CurrencyCodeEUR): entity.CurrencyCodeEUR,
					string(entity.CurrencyCodeCAD): entity.CurrencyCodeCAD,
				},
			},
			&PaymentDateValidator{},
			&PaymentLabelsValidator{
				paymentTypeValues: map[string]entity.PaymentType{
					string(entity.PaymentTypePayin):  entity.PaymentTypePayin,
					string(entity.PaymentTypePayout): entity.PaymentTypePayout,
				},
				expenseTypeValues: map[string]entity.ExpenseType{
					string(entity.ExpenseTypeLegal):     entity.ExpenseTypeLegal,
					string(entity.ExpenseTypeMarketing): entity.ExpenseTypeMarketing,
					string(entity.ExpenseTypeShipment):  entity.ExpenseTypeShipment,
					string(entity.ExpenseTypeFee):       entity.ExpenseTypeFee,
				},
				paymentMethodValues: map[string]entity.PaymentMethod{
					string(entity.PaymentMethodCreditCard):              entity.PaymentMethodCreditCard,
					string(entity.PaymentMethodDebitCard):               entity.PaymentMethodDebitCard,
					string(entity.PaymentMethodElectronicFundsTransfer): entity.PaymentMethodElectronicFundsTransfer,
					string(entity.PaymentMethodBankBillet):              entity.PaymentMethodBankBillet,
					string(entity.PaymentMethodVoucher):                 entity.PaymentMethodVoucher,
					string(entity.PaymentMethodWallet):                  entity.PaymentMethodWallet,
					string(entity.PaymentMethodPix):                     entity.PaymentMethodPix,
					string(entity.PaymentMethodDomesticCard):            entity.PaymentMethodDomesticCard,
					string(entity.PaymentMethodOthers):                  entity.PaymentMethodOthers,
				},
			},
		},
	}
}

func (ref *validator) Validate(params *Params) error {

	errorList := make(map[string]error)
	for _, v := range ref.list {
		abort := v.Validate(params, errorList)
		if abort {
			if err, ok := errorList[wperr.UnavailableError.Error()]; ok {
				return err
			}
			break
		}
	}

	if len(errorList) > 0 {
		return wperr.Business{
			Code:    wperr.BadRequest,
			Message: fmt.Sprintf("Your request is not valid. Check the details: [%v].", errorList),
		}
	}
	return nil
}

func (ref *AmountsValidator) Validate(params *Params, errs map[string]error) bool {

	payment := params.PaymentInput

	ref.validateRequiredFields(payment, errs)
	ref.validateNetValues(payment, errs)
	ref.validatePositiveValues(payment, errs)

	if payment.ExchangeRate != nil {
		if *payment.ExchangeRate == 0 {
			errs["exchange_rate"] = fmt.Errorf("ExchangeRate can't be 0")
		} else {
			ref.validateFormula(payment, errs)
		}
	}

	if payment.ForeignGrossAmount != nil {
		if entity.ProductCategory(payment.ProductCategory) == entity.ProductCategoryGoods && *payment.ForeignGrossAmount > MaxForeignGrossAmount {
			errs["foreign_gross_amount"] = fmt.Errorf("ForeignGrossAmount is greater than 3k with goods as category")
		}
	}

	return false
}

func (ref *AmountsValidator) validateRequiredFields(p PaymentInput, errs map[string]error) {
	if p.ForeignGrossAmount == nil && p.NationalGrossAmount == nil {
		errs["foreign_gross_amount"] = fmt.Errorf("ForeignGrossAmount and NationalGrossAmount can't be both null")
	}

	if p.ForeignGrossAmount == nil && p.ForeignNetAmount != nil {
		errs["foreign_gross_amount"] = fmt.Errorf("ForeignGrossAmount must be informed if ForeignNetAmount is informed")
	}

	if p.NationalGrossAmount == nil && p.NationalNetAmount != nil {
		errs["national_gross_amount"] = fmt.Errorf("NationalGrossAmount must be informed if NationalNetAmount is informed")
	}
}

func (ref *AmountsValidator) validateNetValues(p PaymentInput, errs map[string]error) {
	if p.ForeignNetAmount != nil && p.ForeignGrossAmount != nil && *p.ForeignNetAmount > *p.ForeignGrossAmount {
		errs["foreign_net_amount"] = fmt.Errorf("ForeignNetAmount can't be greater than ForeignGrossAmount")
	}

	if p.NationalNetAmount != nil && p.NationalGrossAmount != nil && *p.NationalNetAmount > *p.NationalGrossAmount {
		errs["national_net_amount"] = fmt.Errorf("NationalNetAmount can't be greater than NationalGrossAmount")
	}

	if p.NationalNetAmount != nil && *p.NationalNetAmount <= 0 {
		errs["national_net_amount"] = fmt.Errorf("NationalNetAmount must be greater than 0 or not provided")
	}

	if p.ForeignNetAmount != nil && *p.ForeignNetAmount <= 0 {
		errs["foreign_net_amount"] = fmt.Errorf("ForeignNetAmount must be greater than 0 or not provided")
	}
}

func (ref *AmountsValidator) validatePositiveValues(p PaymentInput, errs map[string]error) {
	amountFields := map[string]*float64{
		"ForeignGrossAmount":  p.ForeignGrossAmount,
		"ForeignNetAmount":    p.ForeignNetAmount,
		"NationalGrossAmount": p.NationalGrossAmount,
		"NationalNetAmount":   p.NationalNetAmount,
	}

	snakeCaseNames := map[string]string{
		"ForeignGrossAmount":  "foreign_gross_amount",
		"ForeignNetAmount":    "foreign_net_amount",
		"NationalGrossAmount": "national_gross_amount",
		"NationalNetAmount":   "national_net_amount",
	}

	for field, value := range amountFields {
		if value != nil && *value <= 0 {
			errs[snakeCaseNames[field]] = fmt.Errorf("%s must be greater than 0 or not provided", field)
		}
	}
}

func (ref *AmountsValidator) validateFormula(p PaymentInput, errs map[string]error) {
	if p.NationalGrossAmount != nil && p.ForeignGrossAmount != nil {
		if !validateExchangeRate(*p.ForeignGrossAmount, *p.NationalGrossAmount, *p.ExchangeRate) {
			errs["national_gross_amount"] = fmt.Errorf("NationalGrossAmount is not obeying formula [foreignGrossAmount x exchangeRate].")
		}
	}

	if p.NationalNetAmount != nil && p.ForeignNetAmount != nil {
		if !validateExchangeRate(*p.ForeignNetAmount, *p.NationalNetAmount, *p.ExchangeRate) {
			errs["national_net_amount"] = fmt.Errorf("NationalNetAmount is not obeying formula [foreignNetAmount x exchangeRate].")
		}
	}
}

func (ref *ConsumerValidator) Validate(params *Params, errs map[string]error) bool {

	payment := params.PaymentInput
	if entity.ProductCategory(payment.ProductCategory) == entity.ProductCategoryExpenses && payment.Consumer != nil {
		errs["consumer"] = fmt.Errorf("must not provide a consumer. Received: [%v]", payment.Consumer)
		return false
	}

	if entity.ProductCategory(payment.ProductCategory) == entity.ProductCategoryExpenses {
		return false
	}

	if payment.Consumer == nil {
		errs["consumer"] = fmt.Errorf("The Consumer is invalid. Received: [%v]", payment.Consumer)
		return false
	}

	switch entity.ConsumerType(payment.Consumer.Type) {
	case entity.ConsumerTypeIndividual:
		if !wpstr.IsEmpty(payment.Consumer.DateOfBirth) {
			if _, err := validateDate(payment.Consumer.DateOfBirth); err != nil {
				errs["consumer.date_of_birth"] = fmt.Errorf("Date of birth is invalid. Detail: [%w]", err)
			}
		}

		if wpstr.IsEmpty(payment.Consumer.FirstName) {
			errs["consumer.first_name"] = ErrorEmptyFirstName
		}

		if wpstr.IsEmpty(payment.Consumer.LastName) {
			errs["consumer.last_name"] = ErrorEmptyLastName
		}

	default:
		errs["consumer.type"] = fmt.Errorf("The field is invalid. Received: [%s]", payment.Consumer.Type)
	}

	if !wpstr.IsEmpty(payment.Consumer.DocumentValidationDate) {
		docDate, err := validateDate(payment.Consumer.DocumentValidationDate)
		if err != nil {
			errs["consumer.document_validation_date"] = fmt.Errorf("Document validation date is invalid. Detail: [%w].", err)
			return false
		}
		shouldBeValidatedAfter := time.Now().UTC().Add(time.Hour * 24 * 60 * -1)
		if docDate.Before(shouldBeValidatedAfter) {
			errs["consumer.document_validation_date"] = fmt.Errorf("Must not be older than 2 months. Received: [%s]", payment.Consumer.DocumentValidationDate)
		}
	}

	if payment.Consumer.Address != nil {
		errs = validateConsumerAddress(payment.Consumer.Address, errs)
	}

	if !wpstr.IsEmpty(payment.Consumer.Email) {
		errs = validateConsumerEmail(payment.Consumer.Email, errs)
	}

	return false
}

func validateConsumerEmail(email string, errs map[string]error) map[string]error {
	if len(email) > 255 {
		errs["consumer.email"] = fmt.Errorf("email not in the right format")
		return errs
	}

	re := regexp.MustCompile("^[a-zA-Z0-9.!#$%&'*+\\/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$")
	if !re.MatchString(email) {
		errs["consumer.email"] = fmt.Errorf("email not in the right format")
	}

	return errs
}

func validateConsumerAddress(address *Address, errs map[string]error) map[string]error {
	var err error

	err = validateConsumerAddressStreet(address.Street)
	if err != nil {
		errs["consumer.address.street"] = err
	}

	err = validateConsumerAddressNumber(address.Number)
	if err != nil {
		errs["consumer.address.number"] = err
	}

	err = validateConsumerAddressComplement(address.Complement)
	if err != nil {
		errs["consumer.address.complement"] = err
	}

	err = validateConsumerAddressNeighborhood(address.Neighborhood)
	if err != nil {
		errs["consumer.address.neighborhood"] = err
	}

	err = validateConsumerAddressCity(address.City)
	if err != nil {
		errs["consumer.address.city"] = err
	}

	err = validateConsumerAddressCountryCode(address.CountryCode)
	if err != nil {
		errs["consumer.address.country_code"] = err
	}

	err = validateConsumerAddressState(address.State)
	if err != nil {
		errs["consumer.address.state"] = err
	}

	err = validateConsumerAddressZipCode(address.ZipCode)
	if err != nil {
		errs["consumer.address.zip_code"] = err
	}

	return errs
}

func validateConsumerAddressZipCode(code string) error {
	if len(code) < 8 || len(code) > 9 {
		return fmt.Errorf("address zip code not in the right format")
	}

	return nil
}

func validateConsumerAddressState(state string) error {
	if len(state) != 2 {
		return fmt.Errorf("address state not in the right format")
	}

	return nil
}

func validateConsumerAddressCountryCode(code string) error {
	if len(code) < 1 || len(code) > 3 {
		return fmt.Errorf("address country code not in the right format")
	}

	return nil
}

func validateConsumerAddressCity(city string) error {
	if len(city) < 1 || len(city) > 255 {
		return fmt.Errorf("address city not in the right format")
	}

	return nil
}

func validateConsumerAddressNeighborhood(neighborhood string) error {
	if len(neighborhood) > 255 {
		return fmt.Errorf("address neighborhood not in the right format")
	}

	return nil
}

func validateConsumerAddressComplement(complement string) error {
	if len(complement) > 50 {
		return fmt.Errorf("address complement not in the right format")
	}

	return nil
}

func validateConsumerAddressNumber(number string) error {
	if len(number) > 10 {
		return fmt.Errorf("address number not in the right format")
	}

	return nil
}

func validateConsumerAddressStreet(street string) error {
	if len(street) < 1 || len(street) > 255 {
		return fmt.Errorf("address street not in the right format")
	}

	return nil
}

func (ref *CurrencyCodeValidator) Validate(params *Params, errs map[string]error) bool {

	payment := params.PaymentInput
	if _, ok := ref.acceptedValues[payment.CurrencyCode]; !ok {
		var validCurrencies []string
		for c := range ref.acceptedValues {
			validCurrencies = append(validCurrencies, c)
		}
		sort.Sort(sort.Reverse(sort.StringSlice(validCurrencies)))

		errs["currency_code"] = fmt.Errorf("The field is invalid. Received: [%s], Expected one of: %s", payment.CurrencyCode, validCurrencies)
		return false
	}

	return false
}

func (ref *MerchantValidator) Validate(params *Params, errs map[string]error) bool {

	payment := params.PaymentInput
	if wpstr.IsEmpty(payment.MerchantId) {
		errs["merchant_id"] = fmt.Errorf("The field is invalid. Received: [%s]", payment.MerchantId)
		return false
	}

	if payment.MerchantUsedPsp {
		if wpstr.IsEmpty(payment.PspId) {
			errs["psp_id"] = fmt.Errorf("The field is invalid. Received: [%s]", payment.PspId)
			return false
		}
	}
	return false
}

func (ref *PaymentDateValidator) Validate(params *Params, errs map[string]error) bool {

	if params.Date == nil || params.Date.IsZero() {
		errs["date"] = fmt.Errorf("The payment date is invalid. Received: [%v]", params.Date)
	}
	return false
}

func (ref *PaymentLabelsValidator) Validate(params *Params, errs map[string]error) bool {

	payment := params.PaymentInput
	if _, ok := ref.paymentTypeValues[payment.PaymentType]; !ok {
		errs["payment_type"] = fmt.Errorf("The field is invalid. Received: [%s]", payment.PaymentType)
	}

	switch entity.ProductCategory(payment.ProductCategory) {
	case entity.ProductCategoryService:
		if entity.PaymentType(payment.PaymentType) == entity.PaymentTypePayout {
			errs["payment_type"] = ErrorPaymentTypeServices
		}

		if !wpstr.IsEmpty(payment.ExpenseType) {
			errs["expense_type"] = fmt.Errorf("This category has no expense type. Received: [%s]", payment.ExpenseType)
		}

		forbiddenMethods := map[entity.PaymentMethod]interface{}{
			entity.PaymentMethodBankBillet: nil,
			entity.PaymentMethodWallet:     nil,
			entity.PaymentMethodOthers:     nil,
		}
		if _, ok := forbiddenMethods[entity.PaymentMethod(payment.PaymentMethod)]; ok {
			errs["payment_method"] = ErrorInvalidMethodForServices
		}

		if _, ok := ref.paymentMethodValues[payment.PaymentMethod]; !ok {
			errs["payment_method"] = fmt.Errorf("The field is invalid. Received: [%s]", payment.PaymentMethod)
		}

	case entity.ProductCategoryGoods:
		if entity.PaymentType(payment.PaymentType) == entity.PaymentTypePayout {
			errs["payment_type"] = ErrorPaymentTypeGoods
		}

		if !wpstr.IsEmpty(payment.ExpenseType) {
			errs["expense_type"] = fmt.Errorf("This category has no expense type. Received: [%s]", payment.ExpenseType)
		}

		if _, ok := ref.paymentMethodValues[payment.PaymentMethod]; !ok {
			errs["payment_method"] = fmt.Errorf("The field is invalid. Received: [%s]", payment.PaymentMethod)
		}

	case entity.ProductCategoryExpenses:
		if entity.PaymentType(payment.PaymentType) == entity.PaymentTypePayin {
			errs["payment_type"] = ErrorPaymentTypeExpenses
		}

		if entity.ExpenseType(payment.ExpenseType) == entity.ExpenseTypeShipment {
			errs["expense_type"] = ErrorShipmentForExpenses
		}

		if _, ok := ref.expenseTypeValues[payment.ExpenseType]; !ok {
			errs["expense_type"] = fmt.Errorf("The field is invalid. Received: [%s]", payment.ExpenseType)
		}

		if entity.PaymentMethod(payment.PaymentMethod) != entity.PaymentMethodElectronicFundsTransfer {
			errs["payment_method"] = ErrorPayMethodForExpenses
		}

	default:
		errs["product_category"] = fmt.Errorf("The field is invalid. Received: [%s]", payment.ProductCategory)
	}
	return false
}

func validateExchangeRate(foreignAmount, nationalAmount, exchangeRate float64) bool {

	decimalForeign := decimal.NewFromFloat(foreignAmount)
	decimalRate := decimal.NewFromFloat(exchangeRate)
	decimalNational := decimalForeign.Mul(decimalRate)

	calculatedNationalAmount, _ := decimalNational.Truncate(2).Float64()
	if calculatedNationalAmount != nationalAmount {
		diff := calculatedNationalAmount - nationalAmount
		if diff <= -0.02 || diff > 0 {
			return false
		}
	}
	return true
}

func validateDate(value string) (*time.Time, error) {
	date, err := time.Parse(DateLayout, value)
	if err != nil {
		return nil, fmt.Errorf("The date is invalid. The accepted format is: YYYY-MM-DD. Received: [%s]", value)
	}
	return &date, nil
}
