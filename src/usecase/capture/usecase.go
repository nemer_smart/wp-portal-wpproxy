package capture

import (
	"bexstech/wpproxy/src/config"
	"bexstech/wpproxy/src/entity"
	"bexstech/wpproxy/src/usecase/notification"
	"fmt"

	"bitbucket.org/bexstech/wpcore/wperr"
	"bitbucket.org/bexstech/wpcore/wplog"
	"bitbucket.org/bexstech/wpcore/wpstr"
	"go.uber.org/dig"
)

const (
	UuidKey             = "UUID"
	DefaultPaymentTopic = "payment-capture"
)

var (
	log          = wplog.GetLogger("capture")
	paymentTopic string
	newUuidV4    = wpstr.NewUuidV4
)

type useCase struct {
	parser     *parser
	validator  *validator
	mockEngeny *notification.MockEngeny
	cfg        *config.Config
}

func newUseCase(depManager *dig.Container) (*useCase, error) {

	uc := new(useCase)
	if err := depManager.Invoke(func(params struct {
		dig.In

		Conf *config.Config
		Mock *notification.MockEngeny
	}) {
		uc.mockEngeny = params.Mock
		uc.cfg = params.Conf
	}); err != nil {
		panic(err)
	}
	uc.parser = newParser()
	uc.validator = newValidator()
	return uc, nil
}

func (ref *useCase) DispatchPayment(params *Params) (*Response, error) {

	uuid := params.Ctx.Value(UuidKey).(string)
	log.WithField(UuidKey, uuid).Debugf("New payment request received with cancelationId: [%s].", params.LegacyId)

	if wpstr.IsEmpty(params.PaymentInput.MerchantLegalName) {
		return nil, fmt.Errorf("Merchant LegalName must be filled by Accelerator")
	}

	if wpstr.IsEmpty(params.PaymentInput.ProductCategory) {
		// TODO - criar uma logica de ajuste de categoria de produto aqui.
		params.PaymentInput.ProductCategory = string(entity.ProductCategoryGoods)
	}

	if params.Consumer != nil && params.Consumer.Type == string(entity.ConsumerTypeCompany) {
		return nil, wperr.Business{
			Code:    wperr.BadRequest,
			Message: fmt.Sprintf("consumerType %s is temporarily unavailable", params.Consumer.Type),
		}
	}

	if params.ForeignGrossAmount == nil || *params.ForeignGrossAmount == 0 {
		fga := ref.cfg.Simulation.DefaultExchangeRate * (*params.NationalGrossAmount)
		params.ForeignGrossAmount = &fga
	}
	if params.NationalNetAmount == nil || *params.NationalNetAmount == 0 {
		nna := ref.cfg.Simulation.DefaultExchangeRate * (*params.NationalGrossAmount)
		params.NationalNetAmount = &nna
	}
	if params.ForeignNetAmount == nil || *params.ForeignNetAmount == 0 {
		nna := ref.cfg.Simulation.DefaultExchangeRate * (*params.NationalGrossAmount)
		params.ForeignNetAmount = &nna
	}

	//--------------//
	if err := ref.validator.Validate(params); err != nil {
		return nil, err
	}

	payment := ref.parser.parsePayment(params)

	log.WithField(UuidKey, uuid).Debugf("Responding successfully to the new capture. MerchantId: [%s].", payment.PaymentId)
	response := &Response{
		Data: &payment.PaymentRef,
	}
	if payment.ProductCategory == entity.ProductCategoryExpenses {
		switch payment.NationalGrossAmount {
		case ref.cfg.Simulation.ErrorExpenseGrossAmount:
			//@TODO
		case ref.cfg.Simulation.IgnoreExpenseGrossAmount:
			//@TODO
		case ref.cfg.Simulation.RejectExpenseGrossAmount:
			go ref.mockEngeny.SendPaymentRejectNotification(payment, ref.cfg.Simulation.PaymentEventSentWaitTimeMillis)
		default:
			go ref.mockEngeny.SendPaymentAcceptedNotification(payment, ref.cfg.Simulation.PaymentEventSentWaitTimeMillis)
		}
	} else {
		switch payment.NationalGrossAmount {
		case ref.cfg.Simulation.ErrorPaymentGrossAmount:
			//@TODO
		case ref.cfg.Simulation.IgnorePaymentGrossAmount:
			//@TODO
		case ref.cfg.Simulation.RejectPaymentGrossAmount:
			go ref.mockEngeny.SendPaymentRejectNotification(payment, ref.cfg.Simulation.PaymentEventSentWaitTimeMillis)
		default:
			go ref.mockEngeny.SendPaymentAcceptedNotification(payment, ref.cfg.Simulation.PaymentEventSentWaitTimeMillis)
		}
	}
	return response, nil
}
