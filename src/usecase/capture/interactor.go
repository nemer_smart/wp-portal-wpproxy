package capture

import (
	"go.uber.org/dig"
)

type Interactor interface {
	DispatchPayment(params *Params) (*Response, error)
}

func New(depManager *dig.Container) (Interactor, error) {
	return newUseCase(depManager)
}
