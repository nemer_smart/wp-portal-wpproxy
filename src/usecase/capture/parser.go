package capture

import (
	"bexstech/wpproxy/src/entity"
	"bitbucket.org/bexstech/wpcore/wpstr"
	"time"
)

const (
	DateLayout = "2006-01-02"
)

type parser struct{}

func newParser() *parser {

	return new(parser)
}

var (
	timeNow = getTimeNow
)

func (ref *parser) parsePayment(params *Params) *entity.Payment {
	now := timeNow()
	payment := &entity.Payment{
		PaymentRef: entity.PaymentRef{
			PaymentId: newUuidV4(),
			PartnerId: params.PartnerId,
		},
		PaymentOperation: entity.PaymentOperation{
			PaymentTrailer: entity.PaymentTrailer{
				CreatedAt: &now,
				UpdatedAt: &now,
			},
			Amounts: params.PaymentInput.GetAmounts(),
		},
		Date:             params.Date,
		EligibleForHedge: params.EligibleForHedge,
		ExchangeRate:     params.PaymentInput.GetExchangeRate(),
		LegacyId:         params.LegacyId,
		QuoteRequest:     params.QuoteRequest,
		CurrencyCode:     entity.CurrencyCode(params.CurrencyCode),
		PaymentType:      entity.PaymentType(params.PaymentType),
		ProductCategory:  entity.ProductCategory(params.ProductCategory),
		ExpenseType:      entity.ExpenseType(params.ExpenseType),
		PaymentMethod:    entity.PaymentMethod(params.PaymentMethod),
		Merchant: entity.Merchant{
			Id: params.MerchantId,
		},
		MerchantUsedPsp: params.MerchantUsedPsp,
		Psp: &entity.Merchant{
			Id: params.PspId,
		},
	}

	if params.Consumer != nil {
		payment.Consumer = &entity.Consumer{
			Type:           entity.ConsumerType(params.Consumer.Type),
			DocumentNumber: params.Consumer.DocumentNumber,
			DateOfRegistry: params.Consumer.DateOfRegistry,
			FirstName:      params.Consumer.FirstName,
			LastName:       params.Consumer.LastName,
			Email:          params.Consumer.Email,
		}
		if !wpstr.IsEmpty(params.Consumer.DateOfBirth) {
			dateOfBirth, _ := time.Parse(DateLayout, params.Consumer.DateOfBirth)
			payment.Consumer.DateOfBirth = &dateOfBirth
		}
		if !wpstr.IsEmpty(params.Consumer.DocumentValidationDate) {
			documentValidationDate, _ := time.Parse(DateLayout, params.Consumer.DocumentValidationDate)
			payment.Consumer.DocumentValidationDate = &documentValidationDate
		}

		if params.Consumer.Address != nil {
			payment.Consumer.Address = entity.Address{
				Street:       params.Consumer.Address.Street,
				Number:       params.Consumer.Address.Number,
				Complement:   params.Consumer.Address.Complement,
				Neighborhood: params.Consumer.Address.Neighborhood,
				City:         params.Consumer.Address.City,
				CountryCode:  params.Consumer.Address.CountryCode,
				State:        params.Consumer.Address.State,
				ZipCode:      params.Consumer.Address.ZipCode,
			}
		}

		if params.Consumer.Phone != nil {
			payment.Consumer.Phone = entity.Phone{
				Number:      params.Consumer.Phone.Number,
				AreaCode:    params.Consumer.Phone.AreaCode,
				CountryCode: params.Consumer.Phone.CountryCode,
				Provider:    params.Consumer.Phone.Provider,
			}
		}
	}
	return payment
}

func getTimeNow() time.Time {
	return time.Now().Local()
}
