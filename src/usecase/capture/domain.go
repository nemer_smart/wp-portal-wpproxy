package capture

import (
	"bexstech/wpproxy/src/entity"
	"bitbucket.org/bexstech/wpcore/wpmath"
	"context"
	"time"

	"github.com/shopspring/decimal"
)

type Params struct {
	Ctx       context.Context
	PartnerId string
	PaymentInput
}

func (cp *Params) SetPartnerId(partnerId string) {
	cp.PartnerId = partnerId
}

type PaymentInput struct {
	LegacyId            string     `json:"legacy_id,omitempty"`
	Date                *time.Time `json:"date,omitempty"`
	ForeignGrossAmount  *float64   `json:"foreign_gross_amount,omitempty"`
	NationalGrossAmount *float64   `json:"national_gross_amount,omitempty"`
	ForeignNetAmount    *float64   `json:"foreign_net_amount,omitempty"`
	NationalNetAmount   *float64   `json:"national_net_amount,omitempty"`
	ExchangeRate        *float64   `json:"exchange_rate,omitempty"`
	EligibleForHedge    bool       `json:"eligible_for_hedge,omitempty"`
	QuoteRequest        string     `json:"quote_id,omitempty"`
	CurrencyCode        string     `json:"currency_code,omitempty"`
	PaymentType         string     `json:"type"`
	PaymentMethod       string     `json:"payment_method,omitempty"`
	ProductCategory     string     `json:"product_category,omitempty"`
	ExpenseType         string     `json:"expense_type,omitempty"`
	MerchantId          string     `json:"merchant_id,omitempty"`
	MerchantLegalName   string     `json:"merchant_legal_name,omitempty"`
	MerchantUsedPsp     bool       `json:"merchant_used_psp,omitempty"`
	PspId               string     `json:"psp_id,omitempty"`
	Consumer            *Consumer  `json:"consumer,omitempty"`
}

func (p PaymentInput) GetExchangeRate() float64 {
	if p.ExchangeRate != nil {
		return *p.ExchangeRate
	}

	return 0
}

func (p PaymentInput) GetAmounts() (amounts entity.Amounts) {
	amounts.ForeignGrossAmount = p.getForeignGrossAmount()
	amounts.NationalGrossAmount = p.getNationalGrossAmount()
	amounts.ForeignNetAmount = p.getForeignNetAmount()
	amounts.NationalNetAmount = p.getNationalNetAmount()

	return amounts
}

func (p PaymentInput) getForeignGrossAmount() float64 {
	if p.ForeignGrossAmount != nil {
		return *p.ForeignGrossAmount
	} else {
		if p.NationalGrossAmount != nil && p.ExchangeRate != nil {
			return wpmath.Calc(*p.NationalGrossAmount, *p.ExchangeRate, wpmath.CurrencyPrecision, decimal.Decimal.Div)
		}
	}

	return 0
}

func (p PaymentInput) getNationalGrossAmount() float64 {
	if p.NationalGrossAmount != nil {
		return *p.NationalGrossAmount
	} else {
		if p.ForeignGrossAmount != nil && p.ExchangeRate != nil {
			return wpmath.Calc(*p.ForeignGrossAmount, *p.ExchangeRate, wpmath.CurrencyPrecision, decimal.Decimal.Mul)
		}
	}

	return 0
}

func (p PaymentInput) getForeignNetAmount() float64 {
	if p.ForeignNetAmount != nil {
		return *p.ForeignNetAmount
	} else if p.NationalNetAmount != nil && p.ExchangeRate != nil {
		return wpmath.Calc(*p.NationalNetAmount, *p.ExchangeRate, wpmath.CurrencyPrecision, decimal.Decimal.Div)
	} else if p.ForeignGrossAmount != nil {
		return *p.ForeignGrossAmount
	} else if p.NationalGrossAmount != nil && p.ExchangeRate != nil {
		return wpmath.Calc(*p.NationalGrossAmount, *p.ExchangeRate, wpmath.CurrencyPrecision, decimal.Decimal.Div)
	}

	return 0
}

func (p PaymentInput) getNationalNetAmount() float64 {
	if p.NationalNetAmount != nil {
		return *p.NationalNetAmount
	} else if p.ForeignNetAmount != nil && p.ExchangeRate != nil {
		return wpmath.Calc(*p.ForeignNetAmount, *p.ExchangeRate, wpmath.CurrencyPrecision, decimal.Decimal.Mul)
	} else if p.NationalGrossAmount != nil {
		return *p.NationalGrossAmount
	} else if p.ForeignGrossAmount != nil && p.ExchangeRate != nil {
		return wpmath.Calc(*p.ForeignGrossAmount, *p.ExchangeRate, wpmath.CurrencyPrecision, decimal.Decimal.Mul)
	}

	return 0
}

type Consumer struct {
	Type                   string     `json:"type,omitempty"`
	DocumentNumber         string     `json:"document_number,omitempty"`
	DateOfBirth            string     `json:"date_of_birth,omitempty"`
	DateOfRegistry         *time.Time `json:"date_of_Registry,omitempty"`
	FirstName              string     `json:"first_name,omitempty"`
	LastName               string     `json:"last_name,omitempty"`
	Address                *Address   `json:"address,omitempty"`
	Email                  string     `json:"email,omitempty"`
	Phone                  *Phone     `json:"phone,omitempty"`
	DocumentValidationDate string     `json:"document_validation_date,omitempty"`
}

type Address struct {
	Street       string `json:"street,omitempty" validate:"min=1,max=255"`
	Number       string `json:"number,omitempty" validate:"min=0,max=10"`
	Complement   string `json:"complement,omitempty" validate:"min=0,max=50"`
	Neighborhood string `json:"neighborhood,omitempty" validate:"min=0,max=255"`
	City         string `json:"city_name,omitempty" validate:"min=1,max=255"`
	CountryCode  string `json:"country_code,omitempty" validate:"min=1,max=3"`
	State        string `json:"state_code,omitempty" validate:"min=2,max=2"`
	ZipCode      string `json:"zip_code,omitempty" validate:"min=8,max=9"`
}

type Phone struct {
	Number      string `json:"number,omitempty"`
	AreaCode    string `json:"area_code,omitempty"`
	CountryCode string `json:"country_code,omitempty"`
	Provider    string `json:"provider,omitempty"`
}

type Response struct {
	Data *entity.PaymentRef
}
