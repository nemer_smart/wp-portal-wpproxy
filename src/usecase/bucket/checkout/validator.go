package checkout

import (
	"bexstech/wpproxy/src/entity"
	"fmt"

	"bitbucket.org/bexstech/wpcore/wperr"
)

const (
	CheckoutAlReadyExists = "There is already a checkout request for bucket %s with status %s."
)

type validator struct {
	list []Validator
}

type Validator interface {
	Validate(*CheckoutParams) error
}

type (
	CheckoutValidator struct {
	}
)

func newValidator() *validator {

	return &validator{
		list: []Validator{
			&CheckoutValidator{},
		},
	}
}

func (ref *validator) Validate(params *CheckoutParams) error {

	errorList := make(map[string]error)
	for _, v := range ref.list {
		if err := v.Validate(params); err != nil {
			e := err.(*entity.FieldError)
			if e.Abort {
				return e.Message
			}
			errorList[e.Field] = e.Message
		}
	}

	if len(errorList) > 0 {
		return wperr.Business{
			Code:    wperr.BadRequest,
			Message: fmt.Sprintf("Your request is not valid. Check the details: [%v].", errorList),
		}
	}
	return nil
}

func (ref *CheckoutValidator) Validate(params *CheckoutParams) error {

	return nil
}
