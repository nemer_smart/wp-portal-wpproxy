package checkout

import (
	"go.uber.org/dig"
)

type Interactor interface {
	SetBucketCheckout(params *CheckoutParams) (*CheckoutResponse, error)
}

func New(depManager *dig.Container) (Interactor, error) {
	return newUseCase(depManager)
}
