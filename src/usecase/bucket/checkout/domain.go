package checkout

import (
	"context"
)

type CheckoutParams struct {
	Ctx        context.Context `json:"-"`
	PartnerId  string          `json:"-"`
	BucketName string          `json:"-" param:"bucket-name"`
}

func (cp *CheckoutParams) SetPartnerId(partnerId string) {
	cp.PartnerId = partnerId
}

type CheckoutResponse struct {
	Id string `json:"id"`
}
