package checkout

import (
	"bexstech/wpproxy/src/config"
	"bexstech/wpproxy/src/usecase/notification"
	"bitbucket.org/bexstech/wpcore/wplog"
	"go.uber.org/dig"
)

const (
	UuidKey                    = "UUID"
	DefaultBucketCheckoutTopic = "bucket-checkout"
)

var (
	log                 = wplog.GetLogger("buckets.checkout")
	bucketCheckoutTopic string
	hlp                 *helper
)

type useCase struct {
	validator   *validator
	mockeEngeny *notification.MockEngeny
	cfg         *config.Config
}

func newUseCase(depManager *dig.Container) (*useCase, error) {

	uc := new(useCase)
	if err := depManager.Invoke(func(params struct {
		dig.In

		Conf *config.Config
		Mock *notification.MockEngeny
	}) {
		uc.mockeEngeny = params.Mock
		uc.cfg = params.Conf
	}); err != nil {
		panic(err)
	}

	hlp = newHelper()
	return uc, nil
}

func (ref *useCase) SetBucketCheckout(params *CheckoutParams) (*CheckoutResponse, error) {

	uuid := params.Ctx.Value(UuidKey).(string)
	log.WithField(UuidKey, uuid).Debugf("New bucket checkout request received. BucketName: [%s].", params.BucketName)

	checkout := hlp.parseCheckout(params)
	// TODO retornar evento de checkout
	log.WithField(UuidKey, uuid).Debugf("Responding successfully to the new checkout. BucketName: [%s]. CheckoutId: [%s].", checkout.BucketName, checkout.Id)
	response := &CheckoutResponse{
		Id: checkout.Id,
	}
	go ref.mockeEngeny.SendCheckoutEvent(checkout, ref.cfg.Simulation.CheckoutEventSentWaitTimeMillis)
	return response, nil
}
