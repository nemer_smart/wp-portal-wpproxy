package checkout

import (
	"bexstech/wpproxy/src/entity"
	"fmt"
	"time"

	"bitbucket.org/bexstech/wpcore/wpstr"
)

const (
	CheckoutDocPathFormat = "CheckoutOperation/%s/Checkouts/%s"
)

type helper struct{}

func newHelper() *helper {

	return new(helper)
}

func (ref *helper) parseCheckout(params *CheckoutParams) *entity.BucketCheckout {

	now := time.Now()
	return &entity.BucketCheckout{
		Id:         wpstr.NewUuidV4(),
		PartnerId:  params.PartnerId,
		BucketName: params.BucketName,
		Stage:      entity.StageSettlement,
		CreatedAt:  &now,
	}
}

func (hlp *helper) parseCheckoutDocPath(partnerId, bucketName string) string {

	return fmt.Sprintf(CheckoutDocPathFormat, partnerId, bucketName)
}
