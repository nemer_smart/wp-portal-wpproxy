package move

import (
	"bexstech/wpproxy/src/entity"
	"errors"
	"fmt"

	"bitbucket.org/bexstech/wpcore/wperr"
	"bitbucket.org/bexstech/wpcore/wpstr"
)

const (
	OperationIdIncorrectList = "Only a list of ids must be completed, payment_ids: %s, legacy_ids: %s, or cancellation_ids: %s."
	OperationIdExceededList  = "The id list has exceeded the maximum limit of 1000."
	OperationIdEmpty         = "The id list contains empty id. Received: [%s]."
)

type validator struct {
	list []Validator
}

type Validator interface {
	Validate(*MoveParams) error
}

type (
	IdListValidator struct{}

	PaymentMethodValidator struct {
		acceptedValues map[string]entity.PaymentMethod
	}

	PaymentTypeValidator struct {
		acceptedValues map[string]entity.PaymentType
	}

	ProductCategoryValidator struct {
		acceptedValues map[string]entity.ProductCategory
	}
)

func newValidator() *validator {

	return &validator{
		list: []Validator{
			&IdListValidator{},
			&PaymentMethodValidator{
				acceptedValues: map[string]entity.PaymentMethod{
					string(entity.PaymentMethodDomesticCard):            entity.PaymentMethodDomesticCard,
					string(entity.PaymentMethodPix):                     entity.PaymentMethodPix,
					string(entity.PaymentMethodCreditCard):              entity.PaymentMethodCreditCard,
					string(entity.PaymentMethodDebitCard):               entity.PaymentMethodDebitCard,
					string(entity.PaymentMethodElectronicFundsTransfer): entity.PaymentMethodElectronicFundsTransfer,
					string(entity.PaymentMethodBankBillet):              entity.PaymentMethodBankBillet,
					string(entity.PaymentMethodVoucher):                 entity.PaymentMethodVoucher,
					string(entity.PaymentMethodWallet):                  entity.PaymentMethodWallet,
					string(entity.PaymentMethodOthers):                  entity.PaymentMethodOthers,
				},
			},
			&PaymentTypeValidator{
				acceptedValues: map[string]entity.PaymentType{
					string(entity.PaymentTypePayin):  entity.PaymentTypePayin,
					string(entity.PaymentTypePayout): entity.PaymentTypePayout,
				},
			},
			&ProductCategoryValidator{
				acceptedValues: map[string]entity.ProductCategory{
					string(entity.ProductCategoryGoods):    entity.ProductCategoryGoods,
					string(entity.ProductCategoryService):  entity.ProductCategoryService,
					string(entity.ProductCategoryExpenses): entity.ProductCategoryExpenses,
				},
			},
		},
	}
}

func (ref *validator) Validate(params *MoveParams) error {

	errorList := make(map[string]error)
	for _, v := range ref.list {
		if err := v.Validate(params); err != nil {
			e := err.(*entity.FieldError)
			if e.Abort {
				return e.Message
			}
			errorList[e.Field] = e.Message
		}
	}

	if len(errorList) > 0 {
		return wperr.Business{
			Code:    wperr.BadRequest,
			Message: fmt.Sprintf("Your request is not valid. Check the details: [%v].", errorList),
		}
	}
	return nil
}

func (ref *IdListValidator) Validate(params *MoveParams) error {

	var fieldName string
	var amount int
	var operationIdList []string
	totalPayId := len(params.PaymentIds)
	totalCanId := len(params.CancellationIds)
	totalLegId := len(params.LegacyIds)
	switch {
	case totalPayId > 0 && totalCanId == 0 && totalLegId == 0:
		fieldName = "payment_ids"
		amount = totalPayId
		operationIdList = params.PaymentIds

	case totalPayId == 0 && totalCanId > 0 && totalLegId == 0:
		fieldName = "cancellation_ids"
		amount = totalCanId
		operationIdList = params.CancellationIds
	case totalPayId == 0 && totalCanId == 0 && totalLegId > 0:
		fieldName = "legacy_ids"
		amount = totalLegId
		operationIdList = params.LegacyIds

	default:
		return &entity.FieldError{
			Field:   "body",
			Message: fmt.Errorf(OperationIdIncorrectList, params.PaymentIds, params.LegacyIds, params.CancellationIds),
			Abort:   false,
		}
	}

	if amount > 1000 {
		return &entity.FieldError{
			Field:   fieldName,
			Message: errors.New(OperationIdExceededList),
			Abort:   false,
		}
	}

	for _, id := range operationIdList {
		if wpstr.IsEmpty(id) {
			return &entity.FieldError{
				Field:   fieldName,
				Message: fmt.Errorf(OperationIdEmpty, operationIdList),
				Abort:   false,
			}
		}
	}
	return nil
}

func (ref *PaymentMethodValidator) Validate(params *MoveParams) error {

	if wpstr.IsEmpty(params.PaymentMethod) {
		return nil
	}

	if _, ok := ref.acceptedValues[params.PaymentMethod]; !ok {
		return &entity.FieldError{
			Field:   "payment_method",
			Message: fmt.Errorf(entity.InvalidFieldFormat, params.PaymentMethod),
			Abort:   false,
		}
	}
	return nil
}

func (ref *PaymentTypeValidator) Validate(params *MoveParams) error {

	if wpstr.IsEmpty(params.PaymentType) {
		return nil
	}

	if _, ok := ref.acceptedValues[params.PaymentType]; !ok {
		return &entity.FieldError{
			Field:   "payment_type",
			Message: fmt.Errorf(entity.InvalidFieldFormat, params.PaymentType),
			Abort:   false,
		}
	}
	return nil
}

func (ref *ProductCategoryValidator) Validate(params *MoveParams) error {

	if wpstr.IsEmpty(params.ProductCategory) {
		return nil
	}

	if _, ok := ref.acceptedValues[params.ProductCategory]; !ok {
		return &entity.FieldError{
			Field:   "product_category",
			Message: fmt.Errorf(entity.InvalidFieldFormat, params.ProductCategory),
			Abort:   false,
		}
	}
	return nil
}
