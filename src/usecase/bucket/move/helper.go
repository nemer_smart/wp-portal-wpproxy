package move

import (
	"bexstech/wpproxy/src/entity"
	"bitbucket.org/bexstech/wpcore/wpstr"
)

const (
	PaymentMethod   = "PAYMENT_METHOD"
	PaymentType     = "PAYMENT_TYPE"
	ProductCategory = "PRODUCT_CATEGORY"
)

var (
	newUuidV4 = wpstr.NewUuidV4
)

type helper struct{}

func newHelper() *helper {

	return new(helper)
}

func (ref *helper) parseMove(params *MoveParams) *entity.BucketMove {

	operationType := entity.OperationTypePayment
	if len(params.LegacyIds) > 0 {
		operationType = entity.OperationTypeLegacyIds
	} else if len(params.CancellationIds) > 0 {
		operationType = entity.OperationTypeCancellation
	}

	return &entity.BucketMove{
		Id:              newUuidV4(),
		PartnerId:       params.PartnerId,
		TargetBucket:    params.BucketName,
		OperationType:   operationType,
		PaymentIds:      params.PaymentIds,
		CancellationIds: params.CancellationIds,
		LegacyIds:       params.LegacyIds,
	}
}

func (ref *helper) parseFilter(params *MoveParams) map[string]string {

	filter := make(map[string]string)
	if !wpstr.IsEmpty(params.PaymentMethod) {
		filter[PaymentMethod] = params.PaymentMethod
	}
	if !wpstr.IsEmpty(params.PaymentType) {
		filter[PaymentType] = params.PaymentType
	}
	if !wpstr.IsEmpty(params.ProductCategory) {
		filter[ProductCategory] = params.ProductCategory
	}
	return filter
}
