package move

import (
	"bexstech/wpproxy/src/config"
	"bexstech/wpproxy/src/entity"
	"bexstech/wpproxy/src/usecase/notification"
	"bitbucket.org/bexstech/wpcore/wplog"
	"go.uber.org/dig"
)

const (
	UuidKey                = "UUID"
	DefaultBucketMoveTopic = "bucket-move"
)

var (
	log             = wplog.GetLogger("buckets.move")
	bucketMoveTopic string
)

type useCase struct {
	helper      *helper
	validator   *validator
	cfg         *config.Config
	mockeEngeny *notification.MockEngeny
}

func newUseCase(depManager *dig.Container) (*useCase, error) {

	uc := new(useCase)
	if err := depManager.Invoke(func(params struct {
		dig.In

		Conf *config.Config
		Mock *notification.MockEngeny
	}) {
		uc.mockeEngeny = params.Mock
		uc.cfg = params.Conf
	}); err != nil {
		panic(err)
	}

	uc.helper = newHelper()
	uc.validator = newValidator()
	return uc, nil
}

func (ref *useCase) SetBucketMove(params *MoveParams) (*MoveResponse, error) {

	uuid := params.Ctx.Value(UuidKey).(string)
	log.WithField(UuidKey, uuid).Debugf("New bucket move request received. BucketName: [%s].", params.BucketName)

	if err := ref.validator.Validate(params); err != nil {
		return nil, err
	}

	move := ref.helper.parseMove(params)

	// TODO armazenar bucket

	log.WithField(UuidKey, uuid).Debugf("Responding successfully to the new move. BucketName: [%s]. CheckoutId: [%s].", move.TargetBucket, move.Id)
	response := &MoveResponse{
		Id: move.Id,
	}
	// TODO retornar evento
	go ref.mockeEngeny.SendMoveAcceptedNotification(move, 100)
	//go ref.mockeEngeny.SendMoveEntireRejectedNotification(move, 100)

	return response, nil
}

func (ref *useCase) GetBucketBalance(params *BucketBalanceParams) *entity.BucketBalance {
	balance := ref.mockeEngeny.GetBalanceForBucket(params.BucketName)
	return balance
}
