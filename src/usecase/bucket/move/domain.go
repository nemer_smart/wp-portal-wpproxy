package move

import (
	"context"
)

type MoveParams struct {
	Ctx        context.Context `json:"-"`
	PartnerId  string          `json:"-"`
	BucketName string          `json:"-" param:"bucket-name"`
	MoveInput
}

type BucketBalanceParams struct {
	Ctx        context.Context `json:"-"`
	PartnerId  string          `json:"-"`
	BucketName string          `json:"-" param:"bucket-name"`
}

func (mp *BucketBalanceParams) SetPartnerId(partnerId string) {
	mp.PartnerId = partnerId
}

func (mp *MoveParams) SetPartnerId(partnerId string) {
	mp.PartnerId = partnerId
}

type MoveInput struct {
	PaymentIds      []string `json:"payment_ids,omitempty"`
	CancellationIds []string `json:"cancellation_ids,omitempty"`
	LegacyIds       []string `json:"legacy_ids,omitempty"`
	// The following filters will be allowed again in the future.
	PaymentMethod   string `json:"-,omitempty"`
	PaymentType     string `json:"-,omitempty"`
	ProductCategory string `json:"-,omitempty"`
}

type MoveResponse struct {
	Id string `json:"id"`
}
