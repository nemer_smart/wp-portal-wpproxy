package move

import (
	"bexstech/wpproxy/src/entity"
	"go.uber.org/dig"
)

type Interactor interface {
	SetBucketMove(params *MoveParams) (*MoveResponse, error)
	GetBucketBalance(params *BucketBalanceParams) *entity.BucketBalance
}

func New(depManager *dig.Container) (Interactor, error) {
	return newUseCase(depManager)
}
