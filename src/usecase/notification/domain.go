package notification

type EventType string
type PaymentType string
type PaymentMethod string
type ProductCategory string
type ExpenseType string
type ComplianceStatus string
type Stage string
type EventTypeGroup string

const (
	EventTypeCancellationRejected EventType = "CANCELLATION_REJECTED"
	EventTypeCancellationAccepted EventType = "CANCELLATION_ACCEPTED"
	EventTypeMoveAccepted         EventType = "MOVE_ACCEPTED"
	EventTypeMovePartialAccepted  EventType = "MOVE_PARTIAL_ACCEPTED"
	EventTypeMoveRejected         EventType = "MOVE_REJECTED"
	EventTypeRegisterRejected     EventType = "REGISTER_REJECTED"
	EventTypeCheckoutRejected     EventType = "CHECKOUT_REJECTED"
	EventTypeCheckoutSuccess      EventType = "CHECKOUT_SUCCESS"
	EventTypeRegisterSuccess      EventType = "REGISTER_SUCCESS"
	EventTypeRegisterFailed       EventType = "REGISTER_FAILED"
	EventTypePaymentApproved      EventType = "PAYMENT_APPROVED"
	EventTypePaymentInvalidated   EventType = "PAYMENT_INVALIDATED"
	EventTypePaymentRejected      EventType = "PAYMENT_REJECTED"
	EventTypePaymentDuplicated    EventType = "PAYMENT_DUPLICATED"

	StatusAvailableSettlement = "AVAILABLE_SETTLEMENT"
	StatusAvailableRegistry   = "AVAILABLE_REGISTRY"
)

var ValidEvents = []EventType{EventTypeCancellationRejected, EventTypeCancellationAccepted, EventTypeMoveAccepted,
	EventTypeMovePartialAccepted, EventTypeMoveRejected, EventTypeRegisterRejected, EventTypeCheckoutRejected, EventTypeCheckoutSuccess,
	EventTypeRegisterSuccess, EventTypeRegisterFailed, EventTypePaymentApproved, EventTypePaymentInvalidated, EventTypePaymentRejected, EventTypePaymentDuplicated}

const (
	EventTypeGroupPayment      EventTypeGroup = "PAYMENT"
	EventTypeGroupCancellation EventTypeGroup = "CANCELLATION"
	EventTypeGroupMove         EventTypeGroup = "MOVE"
	EventTypeGroupRegister     EventTypeGroup = "REGISTER"
	EventTypeGroupCheckout     EventTypeGroup = "CHECKOUT"
)

/**
THIS MODEL IS THE RETURN FOR THE PARTNER
IT'S NOT THE SAME AS THE CORE MODEL
*/
type EventHeader struct {
	EventType EventType `json:"eventType"`
	EventTime string    `json:"eventTime"`
}

type ContractInfo struct {
	ContractNumber string  `json:"contractNumber"`
	CappingRate    float64 `json:"cappingRate"`
}

type CheckoutRef struct {
	Id   string  `json:"id"`
	Time *string `json:"time"`
}

type BucketRef struct {
	Name  string  `json:"name"`
	Time  *string `json:"time"`
	Stage Stage   `json:"stage"`
}

type ValidationError struct {
	Status    ComplianceStatus  `json:"status,omitempty"`
	ErrorType string            `json:"errorType,omitempty"`
	Message   string            `json:"message,omitempty"`
	Details   map[string]string `json:"details,omitempty"`
}

type PaymentEvent struct {
	EventHeader
	PaymentId           string           `json:"paymentId"`
	Status              string           `json:"status"`
	CreatedAt           *string          `json:"createdAt"`
	UpdatedAt           *string          `json:"updatedAt"`
	Version             int              `json:"version"`
	PartnerId           string           `json:"partnerId"`
	ForeignGrossAmount  float64          `json:"foreignGrossAmount"`
	NationalGrossAmount float64          `json:"nationalGrossAmount"`
	ForeignNetAmount    float64          `json:"foreignNetAmount"`
	NationalNetAmount   float64          `json:"nationalNetAmount"`
	date                string           `json:"date"`
	PaymentType         PaymentType      `json:"paymentType"`
	ExchangeRate        float64          `json:"exchangeRate"`
	CurrencyCode        string           `json:"currencyCode"`
	QuoteRequest        string           `json:"quoteRequest"`
	PaymentMethod       PaymentMethod    `json:"paymentMethod"`
	ProductCategory     ProductCategory  `json:"productCategory"`
	ExpenseType         ExpenseType      `json:"expenseType"`
	MerchantId          string           `json:"merchantId"`
	PspId               *string          `json:"pspId"`
	ContractInfo        *ContractInfo    `json:"contractInfo"`
	ConsumerDocument    *string          `json:"consumerDocument"`
	RegistryCheckout    *CheckoutRef     `json:"registryCheckout"`
	SettlementCheckout  *CheckoutRef     `json:"settlementCheckout"`
	Bucket              *BucketRef       `json:"bucket"`
	Errors              *ValidationError `json:"errors,omitempty"`
	LegacyId            string           `json:"cancelationId"`
}

type CancellationEvent struct {
	EventHeader
	CancellationId      string           `json:"cancellationId"`
	Status              *string          `json:"status"`
	CreatedAt           *string          `json:"createdAt"`
	UpdatedAt           *string          `json:"updatedAt"`
	Version             int              `json:"version"`
	PartnerId           string           `json:"partnerId"`
	ForeignGrossAmount  float64          `json:"foreignGrossAmount"`
	NationalGrossAmount float64          `json:"nationalGrossAmount"`
	ForeignNetAmount    float64          `json:"foreignNetAmount"`
	NationalNetAmount   float64          `json:"nationalNetAmount"`
	date                string           `json:"date"`
	Errors              *ValidationError `json:"errors,omitempty"`
}
type CheckoutEvent struct {
	EventHeader
	ContractNumber  *string        `json:"contractNumber,omitempty"`
	CappingRate     *float64       `json:"cappingRate,omitempty"`
	Stage           *string        `json:"stage,omitempty"`
	BucketName      string         `json:"bucketName"`
	PaymentIds      []string       `json:"paymentIds,omitempty"`
	CancellationIds []string       `json:"cancellationIds,omitempty"`
	Error           *CheckoutError `json:"error,omitempty"`
}

type CheckoutError struct {
	Message string `json:"message"`
}

type MoveEvent struct {
	EventHeader
	BucketName string       `json:"bucketName"`
	Approved   *MoveSection `json:"approved,omitempty"`
	Rejected   *MoveSection `json:"rejected,omitempty"`
	Error      *MoveError   `json:"error,omitempty"`
}

type MoveError struct {
	Message string `json:"message,omitempty"`
	Code    int    `json:"code,omitempty"`
}
type MoveSection struct {
	Message         string   `json:"message"`
	PaymentIds      []string `json:"paymentIds,omitempty"`
	CancellationIds []string `json:"cancellationIds,omitempty"`
}

type PartnerSettings struct {
	PartnerId  string `json:"partnerId"`
	WebHookURL string `json:"webHookURL" firestore:"webHookURL"`
}
