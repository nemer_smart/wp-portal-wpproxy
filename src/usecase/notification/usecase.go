package notification

import (
	"bexstech/wpproxy/src/config"
	"bexstech/wpproxy/src/entity"
	"bitbucket.org/bexstech/wpcore/wplog"
	"bytes"
	"encoding/json"
	cmap "github.com/orcaman/concurrent-map"
	"io/ioutil"
	"net/http"
	"sync"
	"time"
)

type MockEngeny struct {
	cfg             *config.Config
	paymentMap      cmap.ConcurrentMap //map[string]*entity.Payment
	cancellationMap cmap.ConcurrentMap //map[string]*entity.Cancellation
	registeredMap   cmap.ConcurrentMap
	bucketMap       cmap.ConcurrentMap //map[string]*entity.BucketMove
}

var mutex = &sync.Mutex{}

type AutorizationStatus string

const (
	APPROVED                                     = AutorizationStatus("APPROVED")
	REJECTED                                     = AutorizationStatus("REJECTED")
	OPERATION_REJECTED_IN_MOVE_BY_SIMULARION     = "OPERATION_REJECTED_IN_MOVE_BY_SIMULARION"
	OPERATION_REJECTED_IN_CHECKOUT_BY_SIMULARION = "OPERATION_REJECTED_IN_CHECKOUT_BY_SIMULARION"
)

var (
	log = wplog.GetLogger("capture")
)

func NewMockEngeny(cnf *config.Config) *MockEngeny {

	return &MockEngeny{
		cfg:             cnf,
		paymentMap:      cmap.New(), //make(map[string]*entity.Payment),
		registeredMap:   cmap.New(),
		cancellationMap: cmap.New(), //make(map[string]*entity.Cancellation),
		bucketMap:       cmap.New(), //make(map[string]*entity.BucketMove),
	}
}
func (ref *MockEngeny) SendMoveAcceptedNotification(move *entity.BucketMove, waitTime int) {
	// aguarda waitTime para enviar evento de retorno
	time.Sleep(time.Duration(waitTime) * time.Millisecond)
	url := ref.cfg.GetServerConf().NotificationURL
	log.Debug("Notification URL:>", url)
	var apSection = &MoveSection{}
	var rjSection = &MoveSection{}
	if move.OperationType == entity.OperationTypeCancellation {
		apSection.CancellationIds = ref.getOnlyApprovedCancellationsInMoveList(move.CancellationIds)
		rjSection.CancellationIds = ref.getOnlyRejectedCancellationsInMoveList(move.CancellationIds)
	} else {
		apSection.PaymentIds = ref.getOnlyApprovedPaysInMoveList(move.PaymentIds)
		rjSection.PaymentIds = ref.getOnlyRejectedPaysInMoveList(move.PaymentIds)
	}

	event := MoveEvent{
		EventHeader: EventHeader{
			EventType: EventTypeMoveAccepted,
			EventTime: "2021-01-28T18:32:24.358426157-03:00"},
		BucketName: move.TargetBucket,
		Approved:   apSection,
		Rejected:   rjSection,
		Error:      nil,
	}

	ref.ajustBucketEntries(move)

	ref.postEvent(event, url)

}

func (ref *MockEngeny) SendMoveEntireRejectedNotification(move *entity.BucketMove, waitTime int) {
	// aguarda waitTime para enviar evento de retorno
	time.Sleep(time.Duration(waitTime) * time.Millisecond)
	url := ref.cfg.GetServerConf().NotificationURL
	log.Debug("Notification URL:>", url)

	event := MoveEvent{
		EventHeader: EventHeader{
			EventType: EventTypeMoveRejected,
			EventTime: time.Now().String()},
		BucketName: move.TargetBucket,
		Approved:   nil,
		Rejected:   nil,
		Error:      nil,
	}
	ref.postEvent(event, url)

}

func (ref *MockEngeny) getPaymentOrExpense(payId string) *entity.Payment {
	pay, _ := ref.paymentMap.Get(payId)
	return pay.(*entity.Payment)
}

func (ref *MockEngeny) ajustBucketEntries(move *entity.BucketMove) {
	_, ok := ref.bucketMap.Get(move.TargetBucket)
	if !ok {
		ref.bucketMap.Set(move.TargetBucket, &entity.BucketMove{
			Id:              "",
			PartnerId:       move.PartnerId,
			TargetBucket:    move.TargetBucket,
			OperationType:   "",
			PaymentIds:      []string{},
			CancellationIds: []string{},
			LegacyIds:       []string{},
		})
	}

	for _, payId := range move.PaymentIds {
		ref.addPaymentToBucket(ref.getPaymentOrExpense(payId), move.TargetBucket)
	}
	for _, cancId := range move.CancellationIds {
		canc, _ := ref.cancellationMap.Get(cancId)
		ref.addCancellationToBucket(canc.(*entity.Cancellation), move.TargetBucket)
	}
}

func (ref *MockEngeny) SendPaymentAcceptedNotification(payment *entity.Payment, waitTime int) {
	// aguarda waitTime para enviar evento de retorno
	time.Sleep(time.Duration(waitTime) * time.Millisecond)

	url := ref.cfg.GetServerConf().NotificationURL
	log.Debug("Notification URL:>", url)
	if payment.ForeignGrossAmount == 0 {
		payment.ForeignGrossAmount = ref.cfg.Simulation.DefaultExchangeRate * payment.NationalGrossAmount
	}
	if payment.NationalNetAmount == 0 {
		payment.NationalNetAmount = payment.NationalGrossAmount
	}
	if payment.ForeignNetAmount == 0 {
		payment.ForeignNetAmount = payment.ForeignGrossAmount
	}
	var status string
	if payment.ProductCategory == "GOODS" || payment.ProductCategory == "SERVICES" {
		status = StatusAvailableRegistry
	} else {
		status = StatusAvailableSettlement
	}
	pe := PaymentEvent{
		EventHeader: EventHeader{
			EventType: EventTypePaymentApproved,
			EventTime: time.Now().String(),
		},
		PaymentId:           payment.PaymentId,
		Status:              status,
		CreatedAt:           nil,
		UpdatedAt:           nil,
		Version:             0,
		PartnerId:           payment.PartnerId,
		ForeignGrossAmount:  payment.Amounts.ForeignGrossAmount,
		NationalGrossAmount: payment.Amounts.NationalGrossAmount,
		ForeignNetAmount:    payment.Amounts.ForeignNetAmount,
		NationalNetAmount:   payment.Amounts.NationalNetAmount,
		ExchangeRate:        payment.ExchangeRate,
		LegacyId:            payment.LegacyId,
	}
	// mark status for future rejection
	if payment.ProductCategory == entity.ProductCategoryExpenses {
		// analise if the payment must be rejected by gross amount
		if payment.NationalGrossAmount == ref.cfg.Simulation.RejectExpenseInMoveGrossAmount {
			payment.Status = OPERATION_REJECTED_IN_MOVE_BY_SIMULARION
		} else if payment.NationalGrossAmount == ref.cfg.Simulation.RejectExpenseInCheckoutGrossAmount {
			payment.Status = OPERATION_REJECTED_IN_CHECKOUT_BY_SIMULARION
		}
	} else {
		// analise if the payment must be rejected by gross amount
		if payment.NationalGrossAmount == ref.cfg.Simulation.RejectPaymentInMoveGrossAmount {
			payment.Status = OPERATION_REJECTED_IN_MOVE_BY_SIMULARION
		} else if payment.NationalGrossAmount == ref.cfg.Simulation.RejectPaymentInMoveGrossAmount {
			payment.Status = OPERATION_REJECTED_IN_CHECKOUT_BY_SIMULARION
		}
	}
	ref.paymentMap.Set(payment.PaymentId, payment)
	ref.postEvent(pe, url)
	go ref.SendRegistryNotification(payment, 30000)
}

func (ref *MockEngeny) SendRegistryNotification(payment *entity.Payment, waitTime int) {
	// aguarda waitTime para enviar evento de retorno
	time.Sleep(time.Duration(waitTime) * time.Millisecond)
	url := ref.cfg.GetServerConf().NotificationURL
	log.Debug("Notification URL:>", url)
	re := CheckoutEvent{
		EventHeader: EventHeader{
			EventType: EventTypeRegisterSuccess,
			EventTime: time.Now().String(),
		},
		ContractNumber:  nil,
		CappingRate:     nil,
		Stage:           nil,
		BucketName:      "",
		PaymentIds:      []string{payment.PaymentId},
		CancellationIds: nil,
		Error:           nil,
	}
	ref.postEvent(re, url)
}
func (ref *MockEngeny) SendPaymentRejectNotification(payment *entity.Payment, waitTime int) {
	// aguarda waitTime para enviar evento de retorno
	time.Sleep(time.Duration(waitTime) * time.Millisecond)

	url := ref.cfg.GetServerConf().NotificationURL
	log.Debug("Notification URL:>", url)
	pe := PaymentEvent{
		EventHeader: EventHeader{
			EventType: EventTypePaymentRejected,
			EventTime: time.Now().String(),
		},
		PaymentId: payment.PaymentId,
		Status:    "INVALID",
		CreatedAt: nil,
		UpdatedAt: nil,
		Version:   0,
		PartnerId: payment.PartnerId,
		LegacyId:  payment.LegacyId,
	}

	ref.postEvent(pe, url)
}

func (ref *MockEngeny) SendCancelAcceptedNotification(cancel *entity.Cancellation, waitTime int) {
	// aguarda waitTime para enviar evento de retorno
	time.Sleep(time.Duration(waitTime) * time.Millisecond)
	url := ref.cfg.GetServerConf().NotificationURL
	log.Debug("Notification URL:>", url)
	status := "AVAILABLE_REGISTRY"
	if cancel.ForeignGrossAmount == 0 {
		cancel.ForeignGrossAmount = ref.cfg.Simulation.DefaultExchangeRate * cancel.NationalGrossAmount
	}
	if cancel.NationalNetAmount == 0 {
		cancel.NationalNetAmount = cancel.NationalGrossAmount
	}
	if cancel.ForeignNetAmount == 0 {
		cancel.ForeignNetAmount = cancel.ForeignGrossAmount
	}
	pe := CancellationEvent{
		EventHeader: EventHeader{
			EventType: EventTypeCancellationAccepted,
			EventTime: time.Now().String(),
		},
		CancellationId:      cancel.CancellationId,
		Status:              &status,
		CreatedAt:           nil,
		UpdatedAt:           nil,
		Version:             0,
		PartnerId:           cancel.PartnerId,
		ForeignGrossAmount:  cancel.Amounts.ForeignGrossAmount,
		NationalGrossAmount: cancel.Amounts.NationalGrossAmount,
		ForeignNetAmount:    cancel.Amounts.ForeignNetAmount,
		NationalNetAmount:   cancel.Amounts.NationalNetAmount,
	}
	// analise if the payment must be rejected by gross amount
	if cancel.NationalGrossAmount == ref.cfg.Simulation.RejectCancellationInMoveGrossAmount {
		cancel.Status = OPERATION_REJECTED_IN_MOVE_BY_SIMULARION
	} else if cancel.NationalGrossAmount == ref.cfg.Simulation.RejectCancellationInCheckoutGrossAmount {
		cancel.Status = OPERATION_REJECTED_IN_CHECKOUT_BY_SIMULARION
	}
	ref.cancellationMap.Set(cancel.CancellationId, cancel)
	ref.postEvent(pe, url)
}

func (ref *MockEngeny) SendCancelRejectedNotification(cancel *entity.Cancellation, waitTime int) {
	// aguarda waitTime para enviar evento de retorno
	time.Sleep(time.Duration(waitTime) * time.Millisecond)
	url := ref.cfg.GetServerConf().NotificationURL
	log.Debug("Notification URL:>", url)
	status := "AVAILABLE_REGISTRY"
	pe := CancellationEvent{
		EventHeader: EventHeader{
			EventType: EventTypeCancellationRejected,
			EventTime: time.Now().String(),
		},
		CancellationId: cancel.CancellationId,
		Status:         &status,
		CreatedAt:      nil,
		UpdatedAt:      nil,
		Version:        0,
		PartnerId:      cancel.PartnerId,
	}
	ref.postEvent(pe, url)
}

func (ref *MockEngeny) postEvent(pe interface{}, url string) {
	retry := false
	var resp *http.Response
	var err error
	for {
		jsonStr, _ := json.Marshal(&pe)
		req, _ := http.NewRequest("POST", url, bytes.NewBuffer(jsonStr))
		req.Header.Set("Content-Type", "application/json")

		client := &http.Client{}
		resp, err = client.Do(req)
		if err != nil {
			log.WithError(err).Error("Error returning event to the accelerator")
		}
		if resp.StatusCode != 200 {
			log.Warnf("response Status:%s", resp.Status)
			log.Warnf("response Headers:%+v", resp.Header)
			body, _ := ioutil.ReadAll(resp.Body)
			log.Warnf("response Body:%s", string(body))
			if retry {
				log.Infof("Already retried. Forgiven...")
				break
			}
			retry = true
			log.Infof("Realizando retentativa em 5s")
			resp.Body.Close()
			time.Sleep(time.Duration(5) * time.Second)
			continue
		}
		resp.Body.Close()
		break
	}
}

func (ref *MockEngeny) GetBalanceForBucket(bucketName string) *entity.BucketBalance {
	mutex.Lock()
	defer mutex.Unlock()
	b, ok := ref.bucketMap.Get(bucketName)
	if !ok {
		return nil
	}
	bucket := b.(*entity.BucketMove)
	cancs := bucket.CancellationIds
	pays := bucket.PaymentIds

	credits := []*entity.Payment{}
	debits_canc := []*entity.Cancellation{}
	debits_exp := []*entity.Payment{}
	for _, payId := range pays {
		payment := ref.getPaymentOrExpense(payId)
		if payment == nil || payment.Status == OPERATION_REJECTED_IN_MOVE_BY_SIMULARION {
			continue
		}
		if payment.ProductCategory == entity.ProductCategoryExpenses {
			debits_exp = append(debits_exp, payment)
		} else {
			credits = append(credits, payment)
		}
	}

	for _, can := range cancs {
		c, ok := ref.cancellationMap.Get(can)
		cancellation := c.(*entity.Cancellation)
		if !ok || cancellation.Status == OPERATION_REJECTED_IN_MOVE_BY_SIMULARION {
			continue
		}
		debits_canc = append(debits_canc, cancellation)
	}
	balance := entity.BucketBalance{
		PayeeBalanceMap: make(map[string]*entity.PayeeBalance),
	}
	// montando estrutura bucket balance
	// 1o para pagamentos
	for _, pay := range credits {
		pb, ok := balance.PayeeBalanceMap[pay.Merchant.Id]
		if !ok {
			pb = &entity.PayeeBalance{
				CoinBalanceMap: make(map[string]*entity.CoinBalance),
			}
			balance.PayeeBalanceMap[pay.Merchant.Id] = pb
		}
		cb, ok := pb.CoinBalanceMap[string(pay.CurrencyCode)]
		if !ok {
			cb = &entity.CoinBalance{
				GoodsCredits: entity.SellsBalance{
					Credit:               pay.Amounts.ForeignNetAmount,
					TotalCreditRemaining: pay.Amounts.ForeignNetAmount,
				},
				ServicesCredits:       entity.SellsBalance{},
				TakeRateDebits:        nil,
				ExpenseDebits:         []entity.ExpenseDebit{},
				CancellationDebits:    []entity.CancellationDebit{},
				TotalCreditsRemaining: 0,
				TotalDebitsRemaining:  0,
			}
			pb.CoinBalanceMap[string(pay.CurrencyCode)] = cb
		} else {
			cb.GoodsCredits.Credit += pay.Amounts.ForeignNetAmount
			cb.GoodsCredits.TotalCreditRemaining += pay.Amounts.ForeignNetAmount
		}
	}
	// para os expenses
	for _, exp := range debits_exp {
		pb, ok := balance.PayeeBalanceMap[exp.Merchant.Id]
		if !ok {
			pb = &entity.PayeeBalance{
				CoinBalanceMap: make(map[string]*entity.CoinBalance),
			}
			balance.PayeeBalanceMap[exp.Merchant.Id] = pb
		}
		cb, ok := pb.CoinBalanceMap[string(exp.CurrencyCode)]
		if !ok {
			cb = &entity.CoinBalance{
				GoodsCredits:    entity.SellsBalance{},
				ServicesCredits: entity.SellsBalance{},
				TakeRateDebits:  nil,
				ExpenseDebits: []entity.ExpenseDebit{
					{
						Debit:               exp.Amounts.ForeignNetAmount,
						TotalDebitRemaining: exp.Amounts.ForeignNetAmount,
						ExpenseType:         exp.ExpenseType,
					},
				},
				CancellationDebits:    []entity.CancellationDebit{},
				TotalCreditsRemaining: 0,
				TotalDebitsRemaining:  0,
			}
			pb.CoinBalanceMap[string(exp.CurrencyCode)] = cb
		} else {
			cb.ExpenseDebits = append(cb.ExpenseDebits, entity.ExpenseDebit{
				Debit:               exp.Amounts.ForeignNetAmount,
				TotalDebitRemaining: exp.Amounts.ForeignNetAmount,
				ExpenseType:         exp.ExpenseType,
			})
		}
	}

	// para os cancelamento
	for _, canc := range debits_canc {
		p, _ := ref.paymentMap.Get(canc.PaymentId)
		pay := p.(*entity.Payment)
		pb, ok := balance.PayeeBalanceMap[pay.Merchant.Id]
		if !ok {
			pb = &entity.PayeeBalance{
				CoinBalanceMap: make(map[string]*entity.CoinBalance),
			}
			balance.PayeeBalanceMap[pay.Merchant.Id] = pb
		}
		cb, ok := pb.CoinBalanceMap[string(pay.CurrencyCode)]
		if !ok {
			cb = &entity.CoinBalance{
				GoodsCredits:    entity.SellsBalance{},
				ServicesCredits: entity.SellsBalance{},
				TakeRateDebits:  nil,
				ExpenseDebits:   []entity.ExpenseDebit{},
				CancellationDebits: []entity.CancellationDebit{
					{
						Debit:               canc.ForeignNetAmount,
						TotalDebitRemaining: canc.ForeignNetAmount,
						ProductCategory:     pay.ProductCategory,
					},
				},
				TotalCreditsRemaining: 0,
				TotalDebitsRemaining:  0,
			}
			pb.CoinBalanceMap[string(pay.CurrencyCode)] = cb
		} else {
			cb.CancellationDebits = append(cb.CancellationDebits,
				entity.CancellationDebit{
					Debit:               canc.Amounts.ForeignNetAmount,
					TotalDebitRemaining: canc.Amounts.ForeignNetAmount,
					ProductCategory:     pay.ProductCategory,
				})
		}
	}

	// varrer balance para calcular saldos
	for _, payeeBal := range balance.PayeeBalanceMap {
		// calcular debitos

		for _, coinBal := range payeeBal.CoinBalanceMap {
			var debTotal float64
			var credTotal float64
			for _, exp := range coinBal.ExpenseDebits {
				debTotal += exp.Debit
			}
			for _, deb := range coinBal.CancellationDebits {
				debTotal += deb.Debit
			}
			credTotal += coinBal.GoodsCredits.Credit
			if credTotal > debTotal {
				coinBal.TotalCreditsRemaining = credTotal - debTotal
			} else {
				coinBal.TotalDebitsRemaining = debTotal - credTotal
			}
		}

	}
	return &balance

}

func (ref *MockEngeny) addPaymentToBucket(pay *entity.Payment, bucketName string) {
	b, ok := ref.bucketMap.Get(bucketName)
	bucket := b.(*entity.BucketMove)
	if !ok {
		return
	}
	if bucket.PaymentIds == nil {
		bucket.PaymentIds = []string{}
	}
	apps := bucket.PaymentIds

	// se ja existir, faz nada
	for _, p := range apps {
		if p == pay.PaymentId {
			return
		}
	}
	// se existir em outro Bucket, removeElementFromSlice de lá
	if pay.Bucket != nil && pay.Bucket.Name != bucketName {
		ref.removePaymentFromBucket(pay, pay.Bucket.Name)
	}
	pay.Bucket = &entity.BucketRef{
		Name: bucketName,
		Time: nil,
	}
	bucket.PaymentIds = append(apps, pay.PaymentId)
}

func (ref *MockEngeny) removePaymentFromBucket(pay *entity.Payment, bucketName string) {
	b, _ := ref.bucketMap.Get(bucketName)
	bucketOrig := b.(*entity.BucketMove)
	if bucketOrig.PaymentIds == nil {
		return
	}
	for i, _ := range bucketOrig.PaymentIds {
		if bucketOrig.PaymentIds[i] == pay.PaymentId {
			bucketOrig.PaymentIds = removeElementFromSlice(bucketOrig.PaymentIds, i)
			break
		}
	}

}

func (ref *MockEngeny) removeCancellationFromBucket(canc *entity.Cancellation, bucketName string) {
	b, _ := ref.bucketMap.Get(bucketName)
	bucketOrig := b.(*entity.BucketMove)
	if bucketOrig.CancellationIds == nil {
		return
	}
	for i, _ := range bucketOrig.CancellationIds {
		if bucketOrig.CancellationIds[i] == canc.CancellationId {
			bucketOrig.CancellationIds = removeElementFromSlice(bucketOrig.CancellationIds, i)
			break
		}
	}

}

func removeElementFromSlice(slice []string, s int) []string {
	return append(slice[:s], slice[s+1:]...)
}

func (ref *MockEngeny) addCancellationToBucket(canc *entity.Cancellation, bucketName string) {
	b, ok := ref.bucketMap.Get(bucketName)
	if !ok {
		return
	}
	bucket := b.(*entity.BucketMove)
	if bucket.CancellationIds == nil {
		bucket.CancellationIds = []string{}
	}
	cancs := bucket.CancellationIds

	// se ja existir, faz nada
	for _, c := range cancs {
		if c == canc.CancellationId {
			return
		}
	}
	// se existir em outro Bucket, removeElementFromSlice de lá
	if canc.Bucket != nil && canc.Bucket.Name != bucketName {
		ref.removeCancellationFromBucket(canc, canc.Bucket.Name)
	}
	canc.Bucket = &entity.BucketRef{
		Name: bucketName,
		Time: nil,
	}
	bucket.CancellationIds = append(cancs, canc.CancellationId)

}

func (ref *MockEngeny) SendCheckoutEvent(checkout *entity.BucketCheckout, waitTime int) {
	time.Sleep(time.Duration(waitTime) * time.Millisecond)
	url := ref.cfg.GetServerConf().NotificationURL
	log.Debug("Notification URL:>", url)
	b, _ := ref.bucketMap.Get(checkout.BucketName)
	bucket := b.(*entity.BucketMove)
	payIds := ref.getOnlyApprovedPaysInListCheckoutList(bucket.PaymentIds)
	cancIds := ref.getOnlyApprovedCancellationsInCheckoutList(bucket.CancellationIds)
	event := CheckoutEvent{
		EventHeader: EventHeader{
			EventType: EventTypeCheckoutSuccess,
			EventTime: time.Now().String(),
		},
		PaymentIds:      payIds,
		CancellationIds: cancIds,
		BucketName:      bucket.TargetBucket,
		Error:           nil,
	}

	ref.postEvent(event, url)

}

func (ref *MockEngeny) SendCheckoutEntireRejectedEvent(checkout *entity.BucketCheckout, waitTime int) {
	time.Sleep(time.Duration(waitTime) * time.Millisecond)
	url := ref.cfg.GetServerConf().NotificationURL
	log.Debug("Notification URL:>", url)
	b, _ := ref.bucketMap.Get(checkout.BucketName)
	bucket := b.(*entity.BucketMove)
	event := CheckoutEvent{
		EventHeader: EventHeader{
			EventType: EventTypeCheckoutRejected,
			EventTime: time.Now().String()},
		PaymentIds:      bucket.PaymentIds,
		CancellationIds: bucket.CancellationIds,
		BucketName:      bucket.TargetBucket,
		Error:           nil,
	}

	ref.postEvent(event, url)
}

func (ref *MockEngeny) getOnlyApprovedCancellationsInMoveList(ids []string) []string {
	result := []string{}
	for _, id := range ids {
		c, _ := ref.cancellationMap.Get(id)
		cancellation := c.(*entity.Cancellation)
		if cancellation.Status != OPERATION_REJECTED_IN_MOVE_BY_SIMULARION {
			result = append(result, id)
		}
	}
	if len(result) == 0 {
		return nil
	}
	return result
}

func (ref *MockEngeny) getOnlyRejectedCancellationsInMoveList(ids []string) []string {
	result := []string{}
	for _, id := range ids {
		c, _ := ref.cancellationMap.Get(id)
		cancellation := c.(*entity.Cancellation)
		if cancellation.Status == OPERATION_REJECTED_IN_MOVE_BY_SIMULARION {
			result = append(result, id)
		}
	}
	if len(result) == 0 {
		return nil
	}
	return result
}

func (ref *MockEngeny) getOnlyApprovedPaysInMoveList(ids []string) []string {
	result := []string{}
	for _, id := range ids {
		p, _ := ref.paymentMap.Get(id)
		pay := p.(*entity.Payment)
		if pay.Status != OPERATION_REJECTED_IN_MOVE_BY_SIMULARION {
			result = append(result, id)
		}
	}
	if len(result) == 0 {
		return nil
	}
	return result
}

func (ref *MockEngeny) getOnlyRejectedPaysInMoveList(ids []string) []string {
	result := []string{}
	for _, id := range ids {
		p, _ := ref.paymentMap.Get(id)
		pay := p.(*entity.Payment)
		if pay.Status == OPERATION_REJECTED_IN_MOVE_BY_SIMULARION {
			result = append(result, id)
		}
	}
	if len(result) == 0 {
		return nil
	}
	return result
}

func (ref *MockEngeny) getOnlyApprovedCancellationsInCheckoutList(ids []string) []string {
	result := []string{}
	for _, id := range ids {
		_, ok := ref.paymentMap.Get(id)
		if !ok {
			continue
		}
		c, _ := ref.cancellationMap.Get(id)
		cancellation := c.(*entity.Cancellation)
		if cancellation.Status != OPERATION_REJECTED_IN_CHECKOUT_BY_SIMULARION &&
			cancellation.Status != OPERATION_REJECTED_IN_MOVE_BY_SIMULARION {
			result = append(result, id)
		}
	}
	if len(result) == 0 {
		return nil
	}
	return result
}

func (ref *MockEngeny) getOnlyApprovedPaysInListCheckoutList(ids []string) []string {
	result := []string{}
	for _, id := range ids {
		// igora os rejeitados em move
		if _, ok := ref.paymentMap.Get(id); !ok {
			continue
		}
		p, _ := ref.paymentMap.Get(id)
		pay := p.(*entity.Payment)
		if pay.Status != OPERATION_REJECTED_IN_CHECKOUT_BY_SIMULARION &&
			pay.Status != OPERATION_REJECTED_IN_MOVE_BY_SIMULARION {
			result = append(result, id)
		}
	}
	if len(result) == 0 {
		return nil
	}
	return result
}
