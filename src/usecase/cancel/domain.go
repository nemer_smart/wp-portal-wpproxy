package cancel

import (
	"bexstech/wpproxy/src/entity"
	"bitbucket.org/bexstech/wpcore/wperr"
	"bitbucket.org/bexstech/wpcore/wpmath"
	"fmt"
	"github.com/shopspring/decimal"
)

var zero = 0.0

type Params struct {
	PartnerId string `json:"-"`
	PaymentId string `param:"payment-id"`
	Request
}

func (cp *Params) SetPartnerId(partnerId string) {
	cp.PartnerId = partnerId
}

type Request struct {
	Id                  string   `json:"id,omitempty"`
	NationalNetAmount   *float64 `json:"national_net_amount,omitempty"`
	NationalGrossAmount *float64 `json:"national_gross_amount,omitempty"`
	ForeignNetAmount    *float64 `json:"foreign_net_amount,omitempty"`
	ForeignGrossAmount  *float64 `json:"foreign_gross_amount,omitempty"`
}

var decInt = decimal.NewFromInt(100)

const precision = 2

func (cp *Params) calcNetAmount(paymentAmounts entity.Payment) {

	if cp.NationalNetAmount == nil {

		decimalPaymentNationalGrossAmount := decimal.NewFromFloat(paymentAmounts.NationalGrossAmount)
		decimalPaymentNationalNetAmount := decimal.NewFromFloat(paymentAmounts.NationalNetAmount)
		decimalCancelNationalGrossAmount := decimal.NewFromFloat(*cp.NationalGrossAmount)

		propNationalGross := decimalCancelNationalGrossAmount.Div(decimalPaymentNationalGrossAmount)
		nationalNet, _ := (decimalPaymentNationalNetAmount.Mul(propNationalGross)).Round(precision).Float64()
		cp.NationalNetAmount = &nationalNet

	}

	if cp.ForeignNetAmount == nil {

		decimalCancelForeignGross := decimal.NewFromFloat(*cp.ForeignGrossAmount)
		decimalPaymentForeignGross := decimal.NewFromFloat(paymentAmounts.ForeignGrossAmount)

		propForeign := decimalCancelForeignGross.Div(decimalPaymentForeignGross)
		foreignNet, _ := (decimalPaymentForeignGross.Mul(propForeign)).Round(precision).Float64()

		cp.ForeignNetAmount = &foreignNet

	}

}

func (cp *Params) calcGrossAmount(paymentAmounts entity.Payment) {

	if cp.NationalGrossAmount == nil {

		decimalCancelForeignGross := decimal.NewFromFloat(*cp.ForeignGrossAmount)
		decimalExchangeRate := decimal.NewFromFloat(paymentAmounts.ExchangeRate)

		nationalGross, _ := (decimalCancelForeignGross.Mul(decimalExchangeRate)).Round(precision).Float64()
		cp.NationalGrossAmount = &nationalGross

	}

	if cp.ForeignGrossAmount == nil {

		decimalCancelNationalGross := decimal.NewFromFloat(*cp.NationalGrossAmount)
		decimalExchangeRate := decimal.NewFromFloat(paymentAmounts.ExchangeRate)

		foreignGross, _ := (decimalCancelNationalGross.Div(decimalExchangeRate)).Round(precision).Float64()
		cp.ForeignGrossAmount = &foreignGross

	}
}

func (cp *Params) ValidateCalc(paymentValues entity.Payment) error {

	//no values == full cancel, obtaining values from payment
	if (cp.NationalNetAmount == nil || *cp.NationalNetAmount == zero) && (cp.NationalGrossAmount == nil || *cp.NationalGrossAmount == zero) &&
		(cp.ForeignNetAmount == nil || *cp.ForeignNetAmount == zero) && (cp.ForeignGrossAmount == nil || *cp.ForeignGrossAmount == zero) {
		cp.NationalNetAmount = &paymentValues.NationalNetAmount
		cp.NationalGrossAmount = &paymentValues.NationalGrossAmount
		cp.ForeignNetAmount = &paymentValues.ForeignNetAmount
		cp.ForeignGrossAmount = &paymentValues.ForeignGrossAmount
	}

	if cp.ForeignGrossAmount == nil && cp.NationalGrossAmount == nil {
		return wperr.Business{
			Code:    wperr.BadRequest,
			Message: fmt.Sprintf("Received a nil foreign or national gross amount while parsing cancellation."),
		}
	}

	cp.calcGrossAmount(paymentValues)
	cp.calcNetAmount(paymentValues)
	if err := cp.validateFormula(paymentValues.ExchangeRate); err != nil {
		return err
	}
	return nil
}

func (cp *Params) validateFormula(exchangeRate float64) error {

	if !validateExchangeRate(*cp.ForeignNetAmount, *cp.NationalNetAmount, exchangeRate) {
		return wperr.Business{
			Code:    wperr.BadRequest,
			Message: fmt.Sprintf("NationalNetAmount is not obeying formula [foreignNetAmount x exchangeRate]."),
		}
	}

	if !validateExchangeRate(*cp.ForeignGrossAmount, *cp.NationalGrossAmount, exchangeRate) {
		return wperr.Business{
			Code:    wperr.BadRequest,
			Message: fmt.Sprintf("NationalGrossAmount is not obeying formula [foreignGrossAmount x exchangeRate]."),
		}
	}
	return nil
}

func validateExchangeRate(foreignAmount, nationalAmount, exchangeRate float64) bool {
	calculatedNationalAmount := wpmath.Calc(foreignAmount, exchangeRate, wpmath.CurrencyPrecision, decimal.Decimal.Mul)

	if calculatedNationalAmount != nationalAmount {
		diff := calculatedNationalAmount - nationalAmount
		if diff <= -0.02 || diff > 0 {
			return false
		}
	}
	return true
}

func floatPointer(f float64) *float64 {
	return &f
}
