package cancel

import (
	"bexstech/wpproxy/src/entity"
	"bitbucket.org/bexstech/wpcore/wperr"
	"fmt"
	"time"

	"github.com/shopspring/decimal"
)

const (
	CancellationDocPathFormat       = "Payments/%s/payments/%s/operations/%s"
	PaymentDocPathFormat            = "Payments/%s/payments/%s"
	DoubleDecimalPrecision    int32 = -2
)

type parser struct{}

func newParser() *parser {
	return &parser{}
}

func (ref *parser) parseCancellation(params *Params) (*entity.Cancellation, error) {

	if params.ForeignNetAmount == nil || params.NationalNetAmount == nil {
		return nil, wperr.Business{
			Code:    wperr.Internal,
			Message: fmt.Sprintf("Received a nil net amount while parsing cancellation."),
		}
	}

	now := time.Now()
	return &entity.Cancellation{
		CancellationRef: entity.CancellationRef{
			PaymentRef: entity.PaymentRef{
				PartnerId: params.PartnerId,
				PaymentId: params.PaymentId,
			},
			CancellationId: params.Id,
		},
		PaymentOperation: entity.PaymentOperation{
			PaymentTrailer: entity.PaymentTrailer{
				CreatedAt: &now,
			},
			Amounts: entity.Amounts{
				ForeignGrossAmount:  *params.ForeignGrossAmount,
				NationalGrossAmount: *params.NationalGrossAmount,
				ForeignNetAmount:    *params.ForeignNetAmount,
				NationalNetAmount:   *params.NationalNetAmount,
			},
		},
	}, nil
}

func (ref *parser) parseCancelDocPath(partnerId, paymentId, cancellationId string) string {
	return fmt.Sprintf(CancellationDocPathFormat, partnerId, paymentId, cancellationId)
}

func (ref *parser) parsePaymentDocPath(partnerId, paymentId string) string {
	return fmt.Sprintf(PaymentDocPathFormat, partnerId, paymentId)
}

func (ref *parser) parseRound(value float64) string {
	return decimal.NewFromFloatWithExponent(value, DoubleDecimalPrecision).String()
}
