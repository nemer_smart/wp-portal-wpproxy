package cancel

import (
	"bexstech/wpproxy/src/config"
	"bexstech/wpproxy/src/usecase/notification"
	"bitbucket.org/bexstech/wpcore/wplog"
	"context"
	"go.uber.org/dig"
)

const (
	UuidKey = "UUID"
)

var (
	log         = wplog.GetLogger("cancel")
	cancelTopic string
	pars        *parser
)

type useCase struct {
	validator   *validator
	mockeEngeny *notification.MockEngeny `name:"mockEngeny"`
	conf        *config.Config           `name:"conf"`
}

func newUseCase(depManager *dig.Container) (*useCase, error) {
	uc := new(useCase)
	if err := depManager.Invoke(func(params struct {
		dig.In
		Conf *config.Config
		Mock *notification.MockEngeny
	}) {
		uc.mockeEngeny = params.Mock
		uc.conf = params.Conf
	}); err != nil {
		panic(err)
	}
	pars = newParser()
	uc.validator = newValidator()
	return uc, nil
}

func (ref *useCase) DispatchCancel(ctx context.Context, params *Params) error {
	uuid := params.Id
	log.WithField(UuidKey, uuid).Debugf("New cancellation request received. Params: [%v].", params.Request)

	if err := ref.validator.Validate(ctx, params); err != nil {
		return err
	}

	if params.ForeignGrossAmount == nil || *params.ForeignGrossAmount == 0 {
		fga := ref.conf.Simulation.DefaultExchangeRate * (*params.NationalGrossAmount)
		params.ForeignGrossAmount = &fga
	}
	if params.NationalNetAmount == nil || *params.NationalNetAmount == 0 {
		nna := ref.conf.Simulation.DefaultExchangeRate * (*params.NationalGrossAmount)
		params.NationalNetAmount = &nna
	}
	if params.ForeignNetAmount == nil || *params.ForeignNetAmount == 0 {
		nna := ref.conf.Simulation.DefaultExchangeRate * (*params.NationalGrossAmount)
		params.ForeignNetAmount = &nna
	}

	cancellation, err := pars.parseCancellation(params)
	if err != nil {
		log.WithField(UuidKey, uuid).WithError(err).Errorf("Failed parsing cancellation.")
		return err
	}

	log.WithField(UuidKey, uuid).Debugf("Publishing new cancellation. Id: [%v].", cancellation.CancellationRef)

	switch cancellation.ForeignGrossAmount {
	case ref.conf.Simulation.ErrorCancellationGrossAmount:
		//@TODO
	case ref.conf.Simulation.IgnoreCancellationGrossAmount:
		//@TODO
	case ref.conf.Simulation.RejectCancellationGrossAmount:
		go ref.mockeEngeny.SendCancelRejectedNotification(cancellation, ref.conf.Simulation.PaymentEventSentWaitTimeMillis)
	default:
		go ref.mockeEngeny.SendCancelAcceptedNotification(cancellation, ref.conf.Simulation.PaymentEventSentWaitTimeMillis)
	}

	log.WithField(UuidKey, uuid).Debugf("Responding successfully to the new cancellation. CancellationRef: [%v].", cancellation.CancellationRef)
	return nil
}
