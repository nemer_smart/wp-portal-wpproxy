package cancel

import (
	"context"
)

var (
	PaymentInvalidStatusFormat  = "payment can't be cancelled in the current status: %s. Valid status are: %s"
	PaymentNotFoundFormat       = "payment %s doesn't exists"
	ForeignGrossAmountGrtFormat = "the foreign gross amount is greater than %s"
	InvalidRefund               = "category type %s can't be refunded"
)

type validator struct {
	list []Validator
}

type Validator interface {
	Validate(context.Context, *Params) error
}

type (
	PaymentValidator struct {
	}

	FullCancellationValidator struct {
	}
)

func newValidator() *validator {

	return &validator{
		list: []Validator{
			&PaymentValidator{},
			&FullCancellationValidator{},
		},
	}
}

func (ref *validator) Validate(ctx context.Context, params *Params) error {

	return nil
}

func (ref *PaymentValidator) Validate(ctx context.Context, params *Params) error {

	return nil
}

func (ref *FullCancellationValidator) Validate(ctx context.Context, params *Params) error {

	return nil
}
