package cancel

import (
	"context"
	"go.uber.org/dig"
)

type Interactor interface {
	DispatchCancel(ctx context.Context, params *Params) error
}

func New(depManager *dig.Container) (Interactor, error) {
	return newUseCase(depManager)
}
