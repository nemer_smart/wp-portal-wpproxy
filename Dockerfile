#Stage - Build
FROM golang:1.14.6-alpine3.12 AS build
LABEL stage=build
ENV CGO_ENABLED=0 \
    GO111MODULE=on \
    GOOS=linux
WORKDIR /app
COPY . /app
COPY ./id_rsa /root/.ssh/id_rsa
RUN chmod 600 /root/.ssh/id_rsa && \
    mkdir -p /app /dist && \
    apk update && \
    apk upgrade && \
    apk add --no-cache git mercurial openssh-client ca-certificates && \
    git config --global url."git@bitbucket.org:".insteadOf https://bitbucket.org/  && \
    git config --global url."git@github.com:".insteadOf https://github.com/ && \
    ssh-keyscan bitbucket.org >> /root/.ssh/known_hosts && \
    go build -o /dist/main /app/src/main.go

# Stage - Run
FROM alpine:3.12
LABEL stage=run
RUN adduser -S -D -H -h /app appuser
USER appuser
COPY --from=build /dist/main /app/
WORKDIR /app
CMD ["./main"]