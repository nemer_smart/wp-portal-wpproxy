module bexstech/wpproxy

go 1.14

require (
	bitbucket.org/bexstech/wpcore v1.4.1
	github.com/go-playground/validator/v10 v10.4.1
	github.com/ilyakaznacheev/cleanenv v1.2.5
	github.com/kr/pretty v0.1.0 // indirect
	github.com/labstack/echo/v4 v4.2.0
	github.com/orcaman/concurrent-map v0.0.0-20210106121528-16402b402231
	github.com/shopspring/decimal v1.2.0
	github.com/sirupsen/logrus v1.6.0 // indirect
	github.com/stretchr/testify v1.6.1 // indirect
	go.uber.org/dig v1.10.0
	golang.org/x/lint v0.0.0-20200302205851-738671d3881b // indirect
	golang.org/x/mod v0.3.0 // indirect
	golang.org/x/sys v0.0.0-20201015000850-e3ed0017c211 // indirect
	golang.org/x/tools v0.0.0-20200618134242-20370b0cb4b2 // indirect
	gopkg.in/check.v1 v1.0.0-20190902080502-41f04d3bba15 // indirect
	gopkg.in/yaml.v2 v2.3.0 // indirect
)
